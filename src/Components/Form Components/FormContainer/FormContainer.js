import { Button, Grid, Paper} from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import React from "react";
import { NavLink } from "react-router-dom";


const useStyles = makeStyles((theme) => ({
    root: { 
      float:"left",
        borderRadius: 10, 
        borderColor: "#000000", 
        width:"80%",
        marginTop:"30px",
        marginLeft:"120px",
        padding:theme.spacing(2),
     },
     save:{
         marginLeft:theme.spacing(1),
         color:theme.colors.filter
     },
     cancel:{
      textDecoration:"none",
      color:"black"
  },
}));

function FormContainer(props) {

   
    const styles  = useStyles();
    return (
    <Paper className={styles.root}>
    
    <Grid container >
        <Grid item xs={12}></Grid>
        <Grid direction="vertical" item xs={1}></Grid> 
        <Grid item xs={10}>
        <Grid container  >
        {props.children}
        <Grid item xs={8}></Grid>
        <Grid item xs={2}><Button variant="outlined"  className={styles.save}><NavLink className={styles.cancel} to={props.cancel}>CANCEL</NavLink></Button></Grid>
        <Grid item xs={2}><Button variant="outlined" onClick={props.onClick} className={styles.save} >SAVE</Button></Grid>
      </Grid>
     
      
    </Grid>
    </Grid>
    </Paper>
    )

}

export default FormContainer;