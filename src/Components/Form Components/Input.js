import { makeStyles,Button,InputLabel,FormControl, MenuItem, Select, TextField, Grid, RadioGroup, FormControlLabel, Radio, FormLabel } from '@material-ui/core';
import React,{Component} from 'react';
import PublishIcon from '@material-ui/icons/Publish';
import { withStyles } from '@material-ui/styles';
const useStyles =(theme) => ({
	button:{
    height:55,
    paddingRight:theme.spacing(2),
  },
	spacing:{
    paddingBottom:theme.spacing(2),
    paddingRight:theme.spacing(2),
	},
  });
class Input extends Component {
  state={
    error:false,
    helperText:"",
    disabled:false
  }
  
  handleChange=(e)=>{
    if(e.target.value==""){
      this.props.onChange(e.target.value,this.props.name)
      this.setState({error:true,helperText:"This field cant be empty"});

    }else{
      this.props.onChange(e.target.value,this.props.name);

      this.setState({error:false,helperText:"",value:e.target.value});
      
    }
  }
  render(){
    const { classes } = this.props;
    if(this.props.type=="Select"){
  return (
    <>
        <Grid Container className={classes.spacing}>
        <FormControl variant="outlined" fullWidth>
        <InputLabel id="demo-simple-select-outlined-label">{this.props.label}</InputLabel>
        <Select
          labelId="demo-simple-select-outlined-label"
          id="demo-simple-select-outlined"
          onChange={this.handleChange}
          error={this.state.error}  value={this.props.value} 
          helperText={this.state.helperText}
          label={this.props.label} 
          multiple={this.props.multiple!=undefined}
        >
            {
              this.props.options &&
            this.props.options.map((opt)=>{
                return(<MenuItem key={opt.id} value={opt.id}>{opt.name}</MenuItem>)
            })
        }
        </Select>
      </FormControl>
      </Grid>
        
    </>
  
  );
    }
    else if(this.props.type=="File" || this.props.type=="Image"){

      return (
        <>
        <Grid Container className={classes.spacing} fullWidth>
          <Button variant="outlined" className={classes.button} onChange={this.handleChange}  component="label" fullWidth>
             <PublishIcon/>{ "Upload " +this.props.type}
          <input type="file"  hidden/>
          </Button>
          </Grid>
        </>
      )

    }
  else if(this.props.type=="Radio"){
      return (
        <>
        <Grid Container className={classes.spacing} fullWidth>
        <FormControl  >
        <FormLabel >{this.props.label}</FormLabel>
       <RadioGroup color="secondary" name={this.props.label} style={{ display: 'auto' }} value={this.props.value} onChange={this.handleChange} row>
       {
              this.props.options &&
            this.props.options.map((opt)=>{
              
                return(<FormControlLabel checked={this.props.value===opt.id} control={<Radio />} key={opt.id} value={opt.id} label={opt.name}/>)
              
              })
        }
    
  </RadioGroup>
   </FormControl>
          </Grid>
        </>
      )

    }
    else{
        return (
            <>
            <Grid Container className={classes.spacing} fullWidth>
            <TextField  className={classes.inp}  label={this.props.label} variant="outlined" fullWidth value={this.props.value}  onChange={this.handleChange} error={this.state.error} helperText={this.state.helperText}   />
            </Grid>
            </>
          );
    }
}
}
export default withStyles(useStyles)(Input);