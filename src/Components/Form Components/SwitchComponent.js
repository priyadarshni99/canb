import { Grid, makeStyles, Switch } from '@material-ui/core';
import React from 'react';

const useStyles = makeStyles((theme) => ({

  spacing:{
    marginBottom:theme.spacing(3)
  },

}));
export default function SwitchComponent(props) {
    const classes=useStyles();
  return (
    <>
    <Grid item  xs={6} className={classes.spacing}>
      {props.label}
    </Grid>
    <Grid item  xs={6} className={classes.spacing}>
    <Switch
        size="small"
        checked={props.checked}
        onChange={props.onChange}
        color="primary"
      />
    </Grid>
    </>
  );
}
