import { Grid, makeStyles, Switch, TextField } from '@material-ui/core';
import React from 'react';

const useStyles = makeStyles((theme) => ({
  spacing:{
  
      marginRight:theme.spacing(2)
  },
  labelSpacing:{
    marginBottom:theme.spacing(1)
  }

}));
export default function InputSize(props) {
   const classes=useStyles();
 const name=props.name;
 const nameH=name+"Height";
 const nameW=name+"Width";
  return (
    <>
    <Grid container>
     <Grid item className={classes.labelSpacing}  xs={12}>
      {props.label}
    </Grid>
    <Grid item  xs={6}>
    <TextField className={classes.spacing} label="Width" variant="outlined" name={nameW} value={props.valueW} onChange={(e)=>props.onchange(e,nameW)} error={props.errorW} helperText={props.errorTextW} />
    </Grid>
    <Grid item  xs={6}>
    <TextField className={classes.spacing} label="Height" variant="outlined" name={nameH} value={props.valueH}  onChange={(e)=>props.onchange(e,nameH)} error={props.errorH} helperText={props.errorTextH}   />
    </Grid>
    </Grid>
    </>
  );
}