import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Stepper from "@material-ui/core/Stepper";
import Step from "@material-ui/core/Step";
import StepButton from "@material-ui/core/StepButton";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import { Paper, StepLabel } from "@material-ui/core";
import { NavLink } from "react-router-dom";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    borderRadius:"border-radius",
    marginTop:theme.spacing(3),
    marginLeft:theme.spacing(3),
  },
  button: {
    marginRight: theme.spacing(1)
  },
  active: {
    display: "inline-block",
    backgroundColor:"#a7ffeb",
    borderRadius:"4%",

  },
  connector:{
   fontSize:10,

  },
  stepIcon:{
    color:"pink",
  },
  instructions: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1)
  }
}));

function getSteps() {
  const StepperList=[
    {
       name:"SETUP",
       disable:false,
       visible:true,
       link:"/globalParam/setup"
   },
   {
       name:"MY PAGE",
       disable:true,
       visible:true,
       link:"/globalParam/MyPage"
   },
   {
       name:"MY POST",
       disable:false,
       visible:true,
       link:"/globalParam/MyPost"
   },
   {
       name:"MY GROUP",
       disable:false,
       visible:true,
       link:"/"
   },{
       name:"PRODUCT PARAMETER",
       disable:false,
       visible:true,
       link:"/globalParam/ProductParameter"
   },
   {
       name:"CHAT PARAMETER",
       disable:false,
       visible:true,
       link:"/globalParam/ChatParameter"
   },
   {
       name:"NOTIFICATION PARAMETER",
       disable:false,
       visible:true,
       link:"/globalParam/NotificationParameter"
   }
];
  return StepperList;
}

 function StepperMenu() {
  const classes = useStyles();
  //const [activeStep, setActiveStep] = React.useState(0);
  const [completed, setCompleted] = React.useState({});
  const steps = getSteps();

  const handleStep = (step) => () => {
  //  setActiveStep(step);
  };

  return (
    <Paper className={classes.root}>
      <Stepper orientation="vertical" nonLinear  >
        {steps.map((label, index) => {
      const str=window.location.href;
      const link=str.slice(21);
          if(link==label.link ){
            return(<Step className={classes.connector}>
              <StepButton Component={NavLink} onClick={handleStep(index)}  to={label.link} className={classes.active}>  
               <StepLabel>{label.name}</StepLabel>      
              </StepButton>
            </Step>
        )
      } else{
        return(
          <Step className={classes.connector}>
          <StepButton onClick={handleStep(index)}  href={label.link} className={classes.step}>  
           <StepLabel>{label.name}</StepLabel>      
          </StepButton>
        </Step>
      )
      }

        
    })}
      </Stepper>
      <div>
       
      </div>
    </Paper>
  );
}
export default StepperMenu;
