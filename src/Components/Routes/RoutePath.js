const HOME="/";
const ORG_BASIC_SETUP="/orgBasicSetup";
const ORG_BASIC_SETUP_EDIT="/orgBasicSetupForm/:edit/:id";
const ORG_BASIC_SETUP_FORM="/orgBasicSetupForm";

const ORG_BRANCH="/orgBranches";
const ORG_BRANCH_EDIT="/orgBranchForm/:edit/:id";
const ORG_BRANCH_FORM="/orgBranchForm";

const ORG_CLIENTS="/orgClients";
const ORG_CLIENTS_EDIT="/orgClientsForm/:edit/:id";
const ORG_CLIENTS_FORM="/orgClientsForm";

const ORG_TESTIMONIALS="/orgTestimonials";
const ORG_TESTIMONIALS_EDIT="/orgTestimonialForm/:edit/:id";
const ORG_TESTIMONIALS_FORM="/orgTestimonialForm";
export{HOME,
    ORG_BASIC_SETUP,
    ORG_BASIC_SETUP_EDIT,
    ORG_BASIC_SETUP_FORM,
    ORG_BRANCH,
    ORG_BRANCH_EDIT,
    ORG_BRANCH_FORM,
    ORG_CLIENTS,
    ORG_CLIENTS_EDIT,
    ORG_CLIENTS_FORM,
    ORG_TESTIMONIALS,
    ORG_TESTIMONIALS_EDIT,
    ORG_TESTIMONIALS_FORM,
};
