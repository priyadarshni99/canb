import { Grid } from '@material-ui/core'
import React, { Component } from 'react'
import FormContainer from '../../Form Components/FormContainer/FormContainer'
//import ImageInput from '../../Form Components/ImageInput'
import Input from '../../Form Components/Input'

export default class GallariesForm extends Component {
    state={
        image:{
           value:null
        },
        imageName:{
          value:"",
          error:false,
          errorText:""
        }
    }
    handleChange=(e,name)=>{
        if(name=="image"){
            this.setState({[name]:{...[name],value:e.target.files[0]}});
        }else{
            const obj=this.state;
            obj[name].value=e.target.value;
            obj[name].error=e.target.value==""?true:false;
            obj[name].errorText=e.target.value==""?"This field can't be empty":"";
            this.setState({...obj });
        }
        console.log(this.state);
    }

  render() {
      
    return (
      <>
        <FormContainer>
            <Grid item xs={6} >
             <Input type="Image" name="image" label="Image"  onChange={this.handleChange} />
            </Grid>
            <Grid item xs={6} >
            <Input type="Text" label="Image Name" error={this.state.imageName.error} name="imageName"
            errorText={this.state.imageName.errorText}  value={this.state.imageName.value} 
            onChange={this.handleChange}/>
            </Grid>   
        </FormContainer>
      </>
    )
  }
}
