import { Button, Grid, makeStyles, Typography } from "@material-ui/core";
import React, {  useState } from "react";
import { NavLink } from "react-router-dom";
import Download from "../../Common/Download";
import Filter from "../../Common/Filter";
import Search from "../../Common/Search";
import BranchListing from './BranchListing';
import { ORG_BRANCH_FORM } from "../../Routes/RoutePath";
import ButtonTable from "../../Common/ButtonTable";
const useStyles = makeStyles((theme) => ({
  spacing: {
    margin: theme.spacing(1),
  },
  search: {
    margin: theme.spacing(1),
    height: "10",
  },
}));
const BranchListContainer = (props) => {
  const[state,setState] = useState({
    search: null
  });

 const searchHandler = (val) => {
    setState((prevState)=>({...prevState, search: val }));
  };
    const styles = useStyles();
    return (
      <>
        <Grid container className={styles.spacing}>
          <ButtonTable heading="BRANCHES" formLink={ORG_BRANCH_FORM} search={searchHandler}/>
          <BranchListing search={state.search} />
        </Grid>
      </>
    );
  
}
export default BranchListContainer;