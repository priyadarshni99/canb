import { Grid, Modal, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { useHistory } from "react-router";
import {
  createOrgBranches,
  fetchOrgBranchesById,
  fetchOrgSetups,
  updateOrgBranches,
} from "../../../../Api/Api";
import { countries, states } from "../../../../Constants/Constants";
import * as actionTypes from "../../../../store/actions";
import FormHeading from "../../../Common/FormHeading";
import Loading from "../../../Common/Loading";
import FormContainer from "../../../Form Components/FormContainer/FormContainer";
import Input from "../../../Form Components/Input";
import { ORG_BRANCH } from "../../../Routes/RoutePath";
import Status from "../../BasicSetup/Status";
import { BranchValidation } from "../BranchValidation";
const useStyles = makeStyles((theme) => ({
  paper: {
    position: 'absolute',
    width: 400,
    backgroundColor: theme.palette.background.paper,
    border: '2px solid #000',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
  paper: {
    backgroundColor:"white",
    width: 350,
    height:30,
    marginLeft: 400,
    marginTop:150,
    border:'none'
  },
  text:{
    backgroundColor:"white",
    border:'none',
    height:50,
    padding:15
  }
}));
const BranchForm = (props) => {
  const history = useHistory();
  const [state, setState] = useState({
    organisationId: "",
    name: "",
    address: "",
    status: { statusId: 1, effectiveDate: "1067923489", reason: "Works" },
    isPrimary: true,
    countryId: "",
    stateId: "",
    pincode: "",
    latitude: "",
    longitude: "",
    landmark: "",
    nearPublicTransport: "",
  });
  const [organisations,setOrganisations]=useState([]);
  const getOrgNames = async () => {
    await fetchOrgSetups(200, 0).then(({ data }) =>
      setOrganisations(data?.rows)
    );
  };

  const getBranchUpdateData = async () => {
    let id = props.match.params.id;
    await fetchOrgBranchesById(id).then(({ data }) => {
      let status = { stateId: 1, effectiveDate: "32345345", reason: "works" };
      setState((prevState) => ({
        ...prevState,
        organisationId: data.organisationId,
        name: data.name,
        isPrimary: data.isPrimary,
        address: data.address,
        countryId: data.countryId,
        stateId: data.stateId,
        pincode: data.pincode,
        latitude: data.latitude,
        longitude: data.longitude,
        landmark: data.landmark,
        distanceFromLandmark: data.distanceFromLandmark,
        nearPublicTransport: data.nearPublicTransport,
        status: { stateId: 1, effectiveDate: "08/18/2014" }
      }));
    });
  };
  const createBranch = async (data) => {
      if(BranchValidation(data)){
        await createOrgBranches(data, props.match.params.id)
        .then((res) => history.push(ORG_BRANCH));
        }else{
           props.error("Please Enter the data in the right format");
        }
  };
  const updateBranch = async (data) => {
    console.log(data);
    if(BranchValidation(data)){
      await updateOrgBranches(data, props.match.params.id)
      .then((res) => history.push(ORG_BRANCH));
      }else{
         props.error("Please Enter the data in the right format");
      }
  };
  useEffect(() => {
    getOrgNames();
    if (props.match.params.edit) {
      getBranchUpdateData();
    }
  }, []);
  const handleChange = (value, name) => {
  
    setState((prevState) => ({ ...prevState, [name]: value }));
  };
  const handleClick = () => {
    let data = {
      ...state,
      status:{
        ...state.status
      }
    };
    if (props.match.params.edit) {
      updateBranch(data);
    } else {
      createBranch(data);
    }
  };
  const styles = useStyles();
    if(!props.loading){
    return (
      <>
      <FormHeading heading="NEW BRANCH" link={ORG_BRANCH}/>
        <FormContainer onClick={handleClick} cancel={ORG_BRANCH}>
        <Modal
        open={props.modal}
        onClose={()=>props.modalClose()}
        className={styles.paper}>
          <Typography className={styles.text}>{props.errorMessage}</Typography>
      </Modal>
          <Grid item xs={6}>
            <Input
              type="Select"
              options={organisations}
              value={state.organisationId}
              name="organisationId"
              label="organisations"
              onChange={handleChange}
            />
          </Grid>
          <Grid item xs={6}>
            <Input
              type="Text"
              value={state.name}
              name="name"
              label="Branch"
              onChange={handleChange}
            />
          </Grid>
          <Grid item xs={12}>
            <Input
              type="Text"
              name="address"
              value={state.address}
              onChange={handleChange}
              label="Branch Address"
            />
          </Grid>
          <Grid item xs={4}>
            <Input
              type="Select"
              name="countryId"
              options={countries}
              value={state.countryId}
              onChange={handleChange}
              label="Country"
            />
          </Grid>
          <Grid item xs={4}>
            <Input
              type="Select"
              name="stateId"
              options={states}
              value={state.stateId}
              onChange={handleChange}
              label="State"
            />
          </Grid>
          <Grid item xs={4}>
            <Input
              type="Text"
              name="pincode"
              value={state.pincode}
              onChange={handleChange}
              label="Pincode"
            />
          </Grid>
          <Grid item xs={6}>
            <Input
              type="Text"
              name="latitude"
              value={state.latitude}
              onChange={handleChange}
              label="Latitude"
            />
          </Grid>
          <Grid item xs={6}>
            <Input
              type="Text"
              name="longitude"
              value={state.longitude}
              onChange={handleChange}
              label="Longitude"
            />
          </Grid>
          <Grid item xs={6}>
            <Input
              type="Text"
              name="landmark"
              value={state.landmark}
              onChange={handleChange}
              label="Landmark"
            />
          </Grid>
          <Grid item xs={6}>
            <Input
              type="Text"
              name="distanceFromLandmark"
              value={state.distanceFromLandmark}
              onChange={handleChange}
              label="Distance From Landmark"
            />
          </Grid>
          <Grid item xs={6}>
            <Input
              type="Text"
              name="nearPublicTransport"
              value={state.nearPublicTransport}
              onChange={handleChange}
              label="Nearest Bus stop/Railway Station"
            />
          </Grid>
          <Grid item xs={6}></Grid>
          <Status data={state.status} onChange={handleChange} />
        </FormContainer>
      </>
   
    );
    }else{
      return (<Loading/>);
    }
};
const mapStateToProps = (state) => {
  return {
    loading: state.loading,
    errorMessage:state.error,
    modal:state.modal
    };
    };
    const mapDispatchToProps= dispatch =>{
      return{
        modalClose: () => dispatch({type: actionTypes.MODAL_CLOSE}),
        error: (message) => dispatch({type: actionTypes.ERROR,message:message})
      };
      };
export default connect(mapStateToProps,mapDispatchToProps)(BranchForm);
