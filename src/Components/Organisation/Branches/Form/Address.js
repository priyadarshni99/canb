import { Grid } from '@material-ui/core'
import React,{useState,useEffect} from 'react'
import { countries, states } from '../../../../Constants/Constants'
import Input from '../../../Form Components/Input'
const Address = (props) => {
      const [state, setState] = useState({
        isPrimary:true,
        address:"",
        countryId:1,
        stateId:1,
        pincode:null,
        latitude:null,
        longitude:null,
        landmark:null,
        //distanceFromLandmark:null,
        nearPublicTransport:null,
    });
    useEffect(() => {
      console.log(props.data);
        setState({...props.data});
    },[]);
   const handleChange=(value,name)=>{
        setState({...state,[name]:value})
        props.onChange(state,"address");
    }
    return (
      <>
      <Grid item xs={12}>
      <Input type="Text" name="address" value={state.address} onChange={handleChange} label="Branch Address"/>
      </Grid>
      <Grid item xs={4}>
      <Input type="Select" name="countryId" options={countries} value={state.countryId} onChange={handleChange} label="Country"/>
      </Grid>
      <Grid item xs={4}>
      <Input type="Select" name="stateId" options={states} value={state.stateId} onChange={handleChange} label="State"/>
      </Grid>
      <Grid item xs={4}>
      <Input type="Text" name="pincode" value={state.pincode} onChange={handleChange} label="Pincode"/>
      </Grid>
      <Grid item xs={6}>
      <Input type="Text" name="latitude" value={state.latitude} onChange={handleChange} label="Latitude"/>
      </Grid>
      <Grid item xs={6}>
      <Input type="Text" name="longitude" value={state.longitude} onChange={handleChange} label="Longitude"/>
      </Grid>
      <Grid item xs={6}>
      <Input type="Text" name="landmark" value={state.landmark} onChange={handleChange} label="Landmark"/>
      </Grid>
      <Grid item xs={6}>
      <Input type="Text" name="distanceFromLandmark" value={state.distanceFromLandmark} onChange={handleChange} label="Distance From Landmark"/>
      </Grid>
      <Grid item xs={6}>
      <Input type="Text" name="nearPublicTransport" value={state.nearPublicTransport} onChange={handleChange} label="Nearest Bus stop/Railway Station"/>
      </Grid>
    
      </>
    )
}
export default Address;