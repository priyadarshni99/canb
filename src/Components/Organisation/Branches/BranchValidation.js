
export const BranchValidation = (data) =>{
    for(let key in data){
        const value=data[key];
        if(key=="organisationId" && value==""){
            return false;
        }
        else if(key=="organisationId" && Number.isInteger(value)==false){
            return false;
        }
        else if(key=="name" && value==""){
                return false; 
        }
        else if(key=="address" && value==""){
                return false;
        }
        else if(key=="isPrimary" && value==""){
            return false;
        }
        else if(key=="isPrimary" && !(value==true || value==false)){
            return false;
        }
        else if(key=="countryId" && value==""){
                return false;
        }
        else if(key=="countryId" && Number.isInteger(value)==false){
                return false;
            }
        else if(key=="stateId" && value==""){
            return false;
        }
        else if(key=="stateId" && Number.isInteger(value)==false){
                return false;
            }
        else if(key=="pincode" && value==""){
            alert(key,value);
            return false;
        }
        else if(key=="latitude" && value==""){
        return false;
        }
        else if( key=="latitude" && !(isFinite(value) && Math.abs(value) <= 90)){
                return false;
        }
        else if(key=="longitude" && value==""){
            return false;
            }
        else if( key=="longitude" && !(isFinite(value) && Math.abs(value) <= 90)){
                    return false;
            }
        else if(key=="landmark" && value==""){
            return false;
        }
        else if(key=="nearPublicTransport" && value==""){
           return false;
        }
    }
    return true;
}