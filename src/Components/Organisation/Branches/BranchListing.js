import React, { useEffect } from "react";
import {
  fetchOrgBranches,
  searchBranches,
} from "../../../Api/Api";

import TableComponent from "../../Common/TableComponent";
import {  ORG_BRANCH_FORM } from "../../Routes/RoutePath";

const columns = [
  { id: "organisation", label: "Institute Name", minWidth: 170 },
  { id: "name", label: "Branch Name", minWidth: 170 },
  { id: "address", label: "Location", minWidth: 170 },
  { id: "country", label: "Country", minWidth: 170 },
];

function BranchListing(props) {
  const [rows, setRows] = React.useState([]);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  useEffect(() => {
    if (props.search != null) {
      searchBranchesApi(props.search);
    } else {
      fetchOrgBranchesApi();
    }
  },[rowsPerPage,props]);
  const searchBranchesApi = async(search) => {
    await searchBranches(search).then(({ data }) => {
      setRows(data.rows);
      
    });
  };
  const fetchOrgBranchesApi = async() => {
    await fetchOrgBranches(rowsPerPage, 1).then((res) => {
      setRows(res.data.rows);
    });
  };
return(
  <TableComponent rows={rows} columns={columns} link={ORG_BRANCH_FORM} page={(e)=>setRowsPerPage(e)}/>
)
 
}
export default BranchListing;