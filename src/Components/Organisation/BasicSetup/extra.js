import React, { useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TablePagination from "@material-ui/core/TablePagination";
import TableRow from "@material-ui/core/TableRow";
import { Typography, Grid } from "@material-ui/core";
import Ellipses from "./Ellipses";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    margin: "2%",
  },
  container: {
    maxHeight: 440,
  },
  tableHead: {
    height: "56",
    backgroundColor: theme.colors.tableHeader_color,
    fontSize: "16",
    fontStyle:theme.fontStyle.table,
    color: theme.colors.tableHeaderText,
  },
  tableRow: {
    height: "56",
    fontSize: "16",
    backgroundColor: theme.colors.tableHeader_color,
    color: theme.colors.tableRowText,
  },
  tableFooter: {
    height: "56",
    fontSize: "16",
    fontStyle:theme.fontStyle.table,
    backgroundColor: theme.colors.tableFooter,
  },
}));

const TableComponent = (props) => {
  const styles = useStyles();
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [rows, setRows] = React.useState([]);
  const [columns, setColumns] = React.useState([]);
  useEffect(() => {
    setRows(props.rows);
    setColumns(props.columns);
    console.log(columns, rows);
  }, [props]);

  const handleChangePage = (event, newPage) => {
    console.log(newPage);
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    console.log(event.target.value);
    props.page(event.target.value);
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  return (
    <Paper className={styles.root}>
      <TableContainer className={styles.container}>
        <Table stickyHeader>
          <TableHead>
            <TableRow>
              {columns.map((column) => (
                <TableCell
                  className={styles.tableHead}
                  key={column.id}
                  align={column.align}
                  style={{ minWidth: column.minWidth, fontSize: "16px" }}
                >
                  {column.label}
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {rows
              .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              .map((row) => {
                return (
                  <TableRow hover role="checkbox" tabIndex={-1} key={row.code}>
                    {columns.map((column) => {
                      const value =
                        typeof row[column.id] == "object"
                          ? row[column.id].name
                          : row[column.id];

                      if (column.id == columns[0].id) {
                        return (
                          <TableCell
                            key={column.id}
                            align={column.align}
                            className={styles.tableRow}
                            style={{
                              display: "flex",
                              flexWrap: "wrap",
                              fontSize: "12px",
                              padding: "5px",
                            }}
                          >
                            <Grid container>
                              <Grid item xs={8}>
                                <Typography>
                                  {column.format && typeof value === "number"
                                    ? column.format(value)
                                    : value}
                                </Typography>
                              </Grid>
                              <Grid item xs={4}>
                                <Ellipses
                                  id={row.id}
                                  link={props.link}
                                  style={{ flex: 1 }}
                                />
                              </Grid>
                            </Grid>
                          </TableCell>
                        );
                      } else {
                        return (
                          <TableCell
                            key={column.id}
                            align={column.align}
                            style={{ fontSize: "12px", padding: "5px" }}
                          >
                            <Typography>
                              {column.format && typeof value === "number"
                                ? column.format(value)
                                : value}
                            </Typography>
                          </TableCell>
                        );
                      }
                    })}
                  </TableRow>
                );
              })}
          </TableBody>
        </Table>
      </TableContainer>

      <TablePagination
        rowsPerPageOptions={[5, 10, 25, 50, 75, 100]}
        component="div"
        count={rows.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onChangePage={handleChangePage}
        onChangeRowsPerPage={handleChangeRowsPerPage}
      />
    </Paper>
  );
};
export default TableComponent;





























   
import React, { Component } from 'react'
import { areaOfInterest, orgCategories } from '../../../Constants/Constants'
import FormContainer from '../../Form Components/FormContainer/FormContainer'
import Academic from './Academic'
import Status from './Status'
import Url from './Url'
import Input from '../../Form Components/Input'
import SpecialInterest from './SpecialInterest'
import { createOrgBasicSetUp, fetchOrgSetupById, updateOrgBasicSetUp } from '../../../Api/Api'
import B2B from './B2B'
import Company from './Company'
import { Grid } from '@material-ui/core'
import Loading from '../../Common/Loading'
export default class BasicSetupForm extends Component {
  state={
    name:null,
    categoryId:1,
    academic:null,
    specialInterest:null,
    b2b:null,
    company:null,
    url:null,
    orgCategory:null,
    status:null,
    orgCategories:[],
    edit:false,
  }
 
  componentDidMount() {
    this.setState({orgCategories:orgCategories});
    const id=this.props.match.params.id;
    if(id==null){
      this.setState({edit:true})
    }else{
    fetchOrgSetupById(id).then(res=>{
     const data={...res.data};
     console.log(data);
     let academic={
      subCategoryId:data.subCategoryId,
      //boardId:data.boardId,
      classesHandled:data.classesHandled,
      entityId:data.entityId,
      registrationNumber:data.registrationNumber,
      hasBranch:data.hasBranch
     }
     let specialInterest={
      subCategoryId:data.subCategoryId,
      orgAreaOfInterest:data.orgAreaOfInterest,
      minAge:data.minAge,
      maxAge:data.maxAge,
      hasBranch:data.hasBranch,
      entityId:data.entityId,
      registrationNumber:data.registrationNumber
     };
     let b2b={
      subCategoryId:data.subCategoryId,
      establishmentYear:data.establishmentYear,
      buisnessType:data.buisnessType,
      noOfEmployees:data.noOfEmployees,
      industryHandled:data.industryHandled,
      entityId:data.entityId,
      registrationNumber:data.registrationNumber,
      avgTurnover:data.avgTurnover
     }
     let company={
      subCategoryId:data.subCategoryId,
      establishmentYear:data.establishmentYear,
      buisnessType:data.buisnessType,
      noOfEmployees:data.noOfEmployees,
      industryHandled:data.industryHandled,
      entityId:data.entityId,
      registrationNumber:data.registrationNumber,
      turnover:data.turnover,
      avgTurnover:data.avgTurnover
     }  
     let url={
      websiteUrl:data.websiteUrl,
      gstNumber:data.gstNumber,
      taxNumber:data.taxNumber,
      logoUrl:data.logoUrl,
      twitterUrl:data.twitterUrl,
      instagramUrl:data.instagramUrl,
      linkedInUrl:data.linkedInUrl,
      facebookUrl:data.facebookUrl,
      approxusers:data.approxusers,
      optForSelfHosting:data.optForSelfHosting,
      /*serviceProvider:data.serviceProvider,
      userName:data.userName,
      contactPerson:data.contactPerson,
      contactEmail:data.contactEmail,
      contactNumber:data.contactNumber,*/
     }
     let status={
      statusId:data.status.statusId,
      effectiveDate:data.status.effectiveDate
     }
     this.setState({name:data.name,categoryId:data.categoryId,status:status,url:url,academic:academic,specialInterest:specialInterest,b2b:b2b,edit:true})
    })
  }
   
  }
  handleChange=(value,name)=>{
    if(name=="categoryId"){
      let comp=null;
    console.log(this.state.categoryId);
    console.log(this.state.edit);
   if(this.state.categoryId==1){
        console.log("1");
        comp=<Academic data={this.state.academic} onChange={this.handleChange} />;
    }else if(this.state.categoryId==2){
      console.log("2");
      comp=<SpecialInterest data={this.state.specialInterest} onChange={this.handleChange} />;
    }else if(this.state.categoryId==3){
      console.log("3");
      comp=<B2B data={this.state.b2b} onChange={this.handleChange} />;
    }else{
      console.log("4");
      comp=<Company data={this.state.company} onChange={this.handleChange} />
    }
    }
      this.setState({ [name]:value}, () => 
    console.log(this.state))
      console.log(value);
      console.log(name);
     }
 

  render() {
    let comp=null;
    console.log(this.state.categoryId);
    console.log(this.state.edit);
   if(this.state.categoryId==1){
        console.log("1");
        comp=<Academic data={this.state.academic} onChange={this.handleChange} />;
    }else if(this.state.categoryId==2){
      console.log("2");
      comp=<SpecialInterest data={this.state.specialInterest} onChange={this.handleChange} />;
    }else if(this.state.categoryId==3){
      console.log("3");
      comp=<B2B data={this.state.b2b} onChange={this.handleChange} />;
    }else{
      console.log("4");
      comp=<Company data={this.state.company} onChange={this.handleChange} />
    }
if(this.state.edit){
    return (
      <>
        
        <FormContainer onClick={this.handleClick}>
          <Grid item xs={3}>
        <Input type="Text" name="name" label="Organisation Name" value={this.state.name} onChange={this.handleChange}/></Grid>
        <Grid item xs={9}></Grid>
        <Input type="Radio" options={this.state.orgCategories} name="categoryId" label="Category" value={this.state.categoryId} onChange={this.handleChange}/>
        
        
            <Url data={this.state.url} onChange={this.handleChange} />
            <Status data={this.state.status} onChange={this.handleChange}/>
        </FormContainer>
      </>
    )
}else{
  return(<Loading/>)
}
  }
}