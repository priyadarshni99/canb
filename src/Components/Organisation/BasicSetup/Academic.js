import { Grid } from "@material-ui/core";
import React, { useState, useEffect } from "react";
import { fetchOrgClassHandlersBySubCatId } from "../../../Api/Api";
import {
  orgBoards,
  orgEntities,
  orgSubCategories,
} from "../../../Constants/Constants";
import Input from "../../Form Components/Input";

const Academic = (props) => {
  const [state, setState]=useState({
    subCategoryId: 1,
    boardId: 1,
    classesHandled: 1,
    entityId: 1,
    registrationNumber: null,
    hasBranch: false,
    orgClassHandlersBySubCatId: [],
  });

  const getClassesHandled= () =>{
    fetchOrgClassHandlersBySubCatId(state.subCategoryId).then(({data}) =>
    setState((prevState)=>({...prevState, orgClassHandlersBySubCatId: data }))
  );
  }
  useEffect(() => {
    if(props.onClick==true){
      props.onChange(state, "academic");
    }else{
      getClassesHandled();
      setState((prevState)=>({...prevState, ...props.data }));
    } 
  },[props.onClick]);
  const handleChange = (value, name) => {
    if (name == "subCategoryId") {
      setState((prevState)=>({...prevState, [name]: value }));
      getClassesHandled();
    }else{
    setState((prevState)=>({...prevState, [name]: value }));
    }
    
    props.onChange(state, "academic");
  };

  return (
    <>
      <Grid item xs={12}>
        <Input
          type="Select"
          options={orgSubCategories}
          onChange={handleChange}
          value={state.subCategoryId}
          name="subCategoryId"
          label="Sub Category"
        />
      </Grid>
      <Grid item xs={6}>
        <Input
          type="Select"
          options={orgBoards}
          onChange={handleChange}
          value={state.boardId}
          name="boardId"
          label="Board"
        />
      </Grid>
      <Grid item xs={6}>
        <Input
          type="Select"
          options={state.orgClassHandlersBySubCatId}
          onChange={handleChange}
          value={state.orgClassesHandled}
          name="orgClassesHandled"
          label="Classes Handled"
        />
      </Grid>
      <Grid item xs={6}>
        <Input
          type="Select"
          options={orgEntities}
          onChange={handleChange}
          value={state.entityId}
          name="entityId"
          label="Entity"
        />
      </Grid>
      <Grid item xs={6}>
        <Input
          type="text"
          onChange={handleChange}
          value={state.registrationNumber}
          name="registrationNumber"
          label="Registration Number"
        />
      </Grid>
      <Grid item xs={6}>
        <Input
          type="text"
          onChange={handleChange}
          value={state.hasBranch}
          name="hasBranch"
          label="Branch"
        />
      </Grid>
      <Grid item xs={6}></Grid>
    </>
  );
};
export default Academic;
