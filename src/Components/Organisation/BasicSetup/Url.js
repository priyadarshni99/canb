import { Divider, Grid } from "@material-ui/core";
import React, { useEffect,useState } from "react";
import Input from "../../Form Components/Input";

const Url = (props) => {
  const [state,setState] = useState({
    websiteUrl: "",
    gstNumber: "",
    taxNumber: "",
    logoUrl: "",
    twitterUrl: "",
    instagramUrl: "",
    linkedInUrl: "",
    facebookUrl: "",
    approxusers: "",
    optForSelfHosting: false,
    /* serviceProvider:"",
    userName:"",
    contactPerson:"",
    contactEmail:"",
    contactNumber:"",*/
  });
  useEffect(()=>{
    if (props.onClick === true) {
      console.log("url",state);
      props.onChange(state, "url");
    } else {
      setState({ ...props.data });
    }
  },[props.onClick])
  const handleChange = (value, name) => {
    setState((prevState)=>({...prevState, [name]: value }));
    props.onChange(state, "url");
  };
    return (
      <>
        <Grid item xs={6}>
          <Input
            type="text"
            onChange={handleChange}
            value={state.logoUrl}
            name="logoUrl"
            label="Institute Logo"
          />
        </Grid>
        <Grid item xs={6}>
          <Input
            type="text"
            onChange={handleChange}
            value={state.websiteUrl}
            name="websiteUrl"
            label="Organisation Website"
          />
        </Grid>
        <Grid item xs={6}>
          <Input
            type="text"
            onChange={handleChange}
            value={state.gstNumber}
            name="gstNumber"
            label="Gst No"
          />
        </Grid>
        <Grid item xs={6}>
          <Input
            type="text"
            onChange={handleChange}
            value={state.taxNumber}
            name="taxNumber"
            label="Tax Number"
          />
        </Grid>
        <Divider />
        <Grid item xs={6}>
          <Input
            type="text"
            onChange={handleChange}
            value={state.twitterUrl}
            name="twitterUrl"
            label="Twitter Url"
          />
        </Grid>
        <Grid item xs={6}>
          <Input
            type="text"
            onChange={handleChange}
            value={state.instagramUrl}
            name="instagramUrl"
            label="Instagram Url"
          />
        </Grid>
        <Grid item xs={6}>
          <Input
            type="text"
            onChange={handleChange}
            value={state.linkedInUrl}
            name="linkedInUrl"
            label="linkedin Url"
          />
        </Grid>
        <Grid item xs={6}>
          <Input
            type="text"
            onChange={handleChange}
            value={state.facebookUrl}
            name="facebookUrl"
            label="facebook Url"
          />
        </Grid>
        <Divider color="Secondary" />
        <Grid item xs={6}>
          <Input
            type="text"
            onChange={handleChange}
            value={state.approxusers}
            name="approxusers"
            label="Approx Number of Users"
          />
        </Grid>
        <Grid item xs={6}></Grid>
        <Grid item xs={6}>
          <Input
            type="text"
            onChange={handleChange}
            value={state.optForSelfHosting}
            name="optForSelfHosting"
            label="Hosting"
          />
        </Grid>
        <Grid item xs={6}></Grid>
        <Grid item xs={6}>
          <Input
            type="text"
            onChange={handleChange}
            value={state.serviceProvider}
            name="serviceProvider"
            label="Service Provider"
          />
        </Grid>
        <Grid item xs={6}>
          <Input
            type="text"
            onChange={handleChange}
            value={state.userName}
            name="userName"
            label="User Name"
          />
        </Grid>
        <Grid item xs={6}>
          <Input
            type="text"
            onChange={handleChange}
            value={state.contactPerson}
            name="contactPerson"
            label="Contact Person Name"
          />
        </Grid>
        <Grid item xs={6}>
          <Input
            type="text"
            onChange={handleChange}
            value={state.contactEmail}
            name="contactEmail"
            label="Email Id"
          />
        </Grid>
        <Grid item xs={6}>
          <Input
            type="text"
            onChange={handleChange}
            value={state.contactNumber}
            name="contactNumber"
            label="Phone No"
          />
        </Grid>
        <Grid item xs={6}></Grid>
      </>
    );
  
}
export default Url;
