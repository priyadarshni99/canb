import { Grid } from "@material-ui/core";
import React, { useState, useEffect } from "react";
import { status } from "../../../Constants/Constants";
import Input from "../../Form Components/Input";

const Status = (props) => {
  const [state, setState] = useState({
    statusId: 1,
    effectiveDate: "",
    reason: "",
  });
  useEffect(() => {
    if (props.onClick === true) {
      props.onChange(state, "status");
    } else {
      setState({ ...props.data });
    }
  }, [props.onClick]);
  const handleChange = (value, name) => {
    if(name==="statusId"){
      setState({ [name]: parseInt(value) });
    }else{
    setState({ [name]: value });
    props.onChange(state, "status");
    }
  };

  return (
    <>
      <Grid item xs={6}>
        <Input
          type="Radio"
          options={status}
          onChange={handleChange}
          value={state.statusId}
          name="statusId"
          label="status"
        />
      </Grid>
      <Grid item xs={6}>
        <Input
          type="Text"
          onChange={handleChange}
          value={state.effectiveDate}
          name="effectiveDate"
          label="effectiveDate"
        />
      </Grid>
      <Grid item xs={12}>
        <Input
          type="Text"
          onChange={handleChange}
          value={state.reason}
          name="reason"
          label="reason"
        />
      </Grid>
    </>
  );
};
export default Status;
