import { Button, Grid, makeStyles, Typography } from "@material-ui/core";
import React, { useState } from "react";
import { NavLink } from "react-router-dom";
import Download from "../../Common/Download";
import Search from "../../Common/Search";
import ListingScreen from "./ListingScreen";
import Filter from "../../Common/Filter";
import { ORG_BASIC_SETUP_FORM } from "../../Routes/RoutePath";
import Heading from "../../Common/Heading";
import ButtonTable from "../../Common/ButtonTable";
const useStyles = makeStyles((theme) => ({
  spacing: {
    margin: theme.spacing(1),
  },
  search: {
    margin: theme.spacing(1),
    height: "10",
  },
  new:{
     backgroundColor:theme.colors.newButton,
      color: "white",
  }

}));
const ListContainer = (props) => {
  const[state,setState] = useState({
    search: null,
  });

  const searchHandler = (val) => {
    setState({ search: val });
  }
    const styles = useStyles();
    return (
      <>
        <Grid container className={styles.spacing}>
          
           {/*  <Grid xs={2}>
            <Heading>Organisation</Heading>
            </Grid>
            <Grid xs={2}></Grid>
            <Grid xs={3}>
            <Search search={searchHandler} />
            </Grid>
            <Grid xs={2}><Filter /></Grid>
            <Grid xs={2}>
            <Button
              style={{height:"42px",width:"85px"}}
              component={NavLink}
              to={ORG_BASIC_SETUP_FORM}
              className={styles.new}>
              New
            </Button>
            </Grid>
            <Download /> */}
            <Grid xs={12}>
              <ButtonTable heading="ORGANISATION" formLink={ORG_BASIC_SETUP_FORM} search={searchHandler}/>
            </Grid>
            <Grid xs={12}>
            <ListingScreen search={state.search} />
            </Grid>
        </Grid>
      </>
    );
  
}
export default ListContainer;
