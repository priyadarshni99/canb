import React, { useState,useEffect} from "react";
import FormContainer from "../../Form Components/FormContainer/FormContainer";
import Academic from "./Academic";
import Status from "./Status";
import Url from "./Url";
import Input from "../../Form Components/Input";
import SpecialInterest from "./SpecialInterest";
import {
  createOrgBasicSetUp,
  fetchOrgCategories,
  fetchOrgSetupById,
  updateOrgBasicSetUp,
} from "../../../Api/Api";
import B2B from "./B2B";
import Company from "./Company";
import { Grid } from "@material-ui/core";
import Loading from "../../Common/Loading";
import FormHeading from "../../Common/FormHeading";
import { ORG_BASIC_SETUP } from "../../Routes/RoutePath";
const BasicSetupForm =(props)=> {
  const [state,setState] = useState({
    name: "",
    categoryId: 1,
    academic: null,
    specialInterest: null,
    b2b: null,
    company: null,
    url: null,
    status: null,
    orgCategories: []
  });
  const [click,setClick]=useState(false);
  const [edit,setEdit]=useState(false);
  useEffect(() => {
    getOrgCategories();
    if (props.match.params.edit) {
      getUpdateData(props.match.params.id);
    }else{
      setEdit(true);
    }
  }, []);
  const getOrgCategories = async() =>{
   await fetchOrgCategories().then(({data}) =>
    setState({...state,orgCategories: data })
    
  );
  }
  const getUpdateData =async(id) =>{
   await fetchOrgSetupById(id).then((res) => {
      const data = { ...res.data };
      console.log(data);
      let academic = {
        subCategoryId: data.subCategoryId,
        boardId:data.orgBoard.length>0?data.orgBoard[0].boardId:null,
        classesHandled: data.classesHandled,
        entityId: data.entityId,
        registrationNumber: data.registrationNumber,
        hasBranch: data.hasBranch,
      };
      let specialInterest = {
        subCategoryId: data.subCategoryId,
        orgAreaOfInterest: data.orgAreaOfInterest,
        minAge: data.minAge,
        maxAge: data.maxAge,
        hasBranch: data.hasBranch,
        entityId: data.entityId,
        registrationNumber: data.registrationNumber,
      };
      let b2b = {
        subCategoryId: data.subCategoryId,
        establishmentYear: data.establishmentYear,
        businessTypeId: data.businessTypeId,
        noOfEmployees: data.noOfEmployees,
        industryHandled: data.industryHandled,
        entityId: data.entityId,
        registrationNumber: data.registrationNumber,
        avgTurnover: data.avgTurnover,
      };
      let company = {
        subCategoryId: data.subCategoryId,
        establishmentYear: data.establishmentYear,
        businessTypeId: data.businessTypeId,
        noOfEmployees: data.noOfEmployees,
        industryHandled: data.industryHandled,
        entityId: data.entityId,
        registrationNumber: data.registrationNumber,
        turnover: data.turnover,
        avgTurnover: data.avgTurnover,
      };
      let url = {
        websiteUrl: data.websiteUrl,
        gstNumber: data.gstNumber,
        taxNumber: data.taxNumber,
        logoUrl: data.logoUrl,
        twitterUrl: data.twitterUrl,
        instagramUrl: data.instagramUrl,
        linkedInUrl: data.linkedInUrl,
        facebookUrl: data.facebookUrl,
        approxusers: data.approxusers,
        optForSelfHosting: data.optForSelfHosting,
      };
      let status = {
        statusId: 1,
        effectiveDate: "12345678",
      };
      setState((prevState)=>({
        ...prevState,
        name: data.name,
        categoryId: data.categoryId,
        status: status,
        url: url,
        academic: academic,
        specialInterest: specialInterest,
        b2b: b2b,
        company: company,
      }));
    });
    setEdit(true);
  }
  const displayForm =()=>{
    setEdit(true);
      alert(edit);
  }
  
 
  const updatedData = () =>{
    let orgCategory = state.orgCategories.find(
      (item) => item.id == state.categoryId
    ).name;
    if (orgCategory == "Academic") {
      let acad = {
        subCategoryId: state.academic.subCategoryId,
        orgBoard: [{ boardId: "1" }],
        classesHandled: state.academic.classesHandled,
        entityId: state.academic.entityId,
        registrationNumber: state.academic.registrationNumber,
        hasBranch: state.academic.hasBranch,
      };
      setState({...state,academic: { ...acad } });
      console.log(state.academic);
    } else if (orgCategory == "Special Interest") {
      const aoi = state.specialInterest.orgAreaOfInterest.map((aoi) => {
        return { areaOfInterestId: aoi };
      });
      let sp = {
        subCategoryId: state.specialInterest.subCategoryId,
        orgAreaOfInterest: [...aoi],
        minAge: state.specialInterest.minAge,
        maxAge: state.specialInterest.maxAge,
        hasBranch: state.specialInterest.hasBranch,
        entityId: state.specialInterest.entityId,
        registrationNumber: state.specialInterest.registrationNumber,
      };
      setState(
        (prevState) => ({
          ...prevState,
          specialInterest: {
            ...prevState.specialInterest,
            orgAreaOfInterest: [...aoi],
          }
        })
      );
      console.log("Special interest data",state);
    } else if (orgCategory == "B2B Network") {
      let st = state.b2b;
      const averageTurnover = [
        { year: st.year1, turnover: st.turnover1 },
        { year: st.year2, turnover: st.turnover2 },
        { year: st.year3, turnover: st.turnover3 },
      ];

      let b2b = {
        subCategoryId: state.b2b.subCategoryId,
        establishmentYear: state.b2b.establishmentYear,
        businessTypeId: state.b2b.businessTypeId,
        noOfEmployees: state.b2b.noOfEmployees,
        industryHandled: state.b2b.industryHandled,
        entityId: state.b2b.entityId,
        registrationNumber: state.b2b.registrationNumber,
        avgTurnover: [...averageTurnover],
      };
      setState((prevState) => ({ ...prevState, b2b: { ...b2b } }));
      console.log("B2B avg",averageTurnover);
      console.log("B2B data",state.b2b);
    } else if (orgCategory == "Company") {
      let st = state.company;
      const avgTurnover = [
        { year: st.year1, turnover: st.turnover1 },
        { year: st.year2, turnover: st.turnover2 },
        { year: st.year3, turnover: st.turnover3 },
      ];

      let company = {
        subCategoryId: state.company.subCategoryId,
        establishmentYear: state.company.establishmentYear,
        buisnessTypeId: state.company.buisnessTypeId,
        noOfEmployees: state.company.noOfEmployees,
        industryHandled: state.company.industryHandled,
        entityId: state.company.entityId,
        registrationNumber: state.company.registrationNumber,
        avgTurnover: [...avgTurnover],
      };

      setState((prevState) => ({ ...prevState, company: {...company } }));
    }
  }
 const handleClick = () => {
   setClick(true);
  
    updatedData();
    let cat = null;
    if (state.categoryId == 1) {
      cat = { ...state.academic };
    } else if (state.categoryId == 2) {
      cat = { ...state.specialInterest };
    } else if (state.categoryId == 3) {
      cat = { ...state.b2b };
    } else if (state.categoryId == 4) {
      cat = { ...state.company };
    }
    let data = {
      name: state.name,
      categoryId: state.categoryId,
      ...state.url,
      ...cat,
      status: {
        statusId: 1,
        effectiveDate: "123456",
      },
    };
    console.log("data before submitting",data);
    if (props.match.params.edit) {
      updateOrgBasicSetUpApi(data);
    } else {
      createOrgBasicSetUpApi(data);
    }
  }
 const handleChange = (value, name) => {
    if (name === "categoryId") {
      setState({...state,[name]: parseInt(value) });
    } else {
      setState({...state, [name]: value });
    }
  }
  const createOrgBasicSetUpApi = async(data) => {
    await createOrgBasicSetUp(data)
    .then((res) => alert("works"))
    .catch((err) => console.log(err));
}
 const updateOrgBasicSetUpApi = async(data) => {
    await updateOrgBasicSetUp(data, props.match.params.id)
    .then((res) => alert("works"))
    .catch((err) => console.log(err));
   
  }

 const renderRadioComp = {
    1: <Academic data={state.academic} onChange={handleChange} onClick={click} />,
    2: (
      <SpecialInterest
        data={state.specialInterest}
        onChange={handleChange}
        onClick={click}
      />
    ),
    3: <B2B data={state.b2b} onChange={handleChange} onClick={click} />,
    4: <Company data={state.company} onChange={handleChange} onClick={click} />,
  };

    if (edit) {
      return (
        <>
        <FormHeading heading="NEW ORGANISATION" link={ORG_BASIC_SETUP} />
          <FormContainer onClick={handleClick} cancel={ORG_BASIC_SETUP}>
            <Grid item xs={4}>
              <Input
                type="Text"
                name="name"
                label="Organisation Name"
                value={state.name}
                onChange={handleChange}
              />
            </Grid>
            <Grid item xs={8}></Grid>
            <Input
              type="Radio"
              options={state.orgCategories}
              name="categoryId"
              label="Category"
              value={state.categoryId}
              onChange={handleChange}
            />
            {renderRadioComp[state.categoryId]}
            <Url data={state.url} onChange={handleChange} onClick={click}/>
            <Status data={state.status} onChange={handleChange} onClick={click} />
          </FormContainer>
        </>
      );
    } else {
      return <Loading />;
    }
  
  }
export default BasicSetupForm;