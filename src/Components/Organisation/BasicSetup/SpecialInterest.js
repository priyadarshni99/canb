import { Grid } from "@material-ui/core";
import React, { useState, useEffect } from "react";
import { fetchOrgClassHandlersBySubCatId } from "../../../Api/Api";
import {
  areaOfInterest,
  orgEntities,
  orgSubCategories,
} from "../../../Constants/Constants";
import Input from "../../Form Components/Input";
let orgClassHandlersBySubCatId = null;
const SpecialInterest = (props) => {
  const [state, setState] = useState({
    subCategoryId: 1,
    orgAreaOfInterest: [],
    minAge: null,
    maxAge: null,
    hasBranch: false,
    entityId: 1,
    registrationNumber: null,
  });
  useEffect(() => {
    if (props.onClick === true) {
      props.onChange(state, "specialInterest");
    } else if (props.data != null) {
      console.log(props.data);
      let orgAOI = [];
      orgAOI =
        props.data.orgAreaOfInterest.length >= 0
          ? props.data.orgAreaOfInterest.map((aoi) => {
              return aoi.areaOfInterestId;
            })
          : null;
      setState({
        ...props.data,
        orgAreaOfInterest: orgAOI,
        subCategoryId: props.data.subCategoryId,
      });
    }
  }, [props.onClick]);
  const handleChange = (value, name) => {
    setState((prevState) => ({ ...prevState, [name]: value }));

    props.onChange(state, "specialInterest");
  };
  return (
    <>
      <Grid item xs={12}>
        <Input
          type="Select"
          options={orgSubCategories}
          onChange={handleChange}
          value={state.subCategoryId}
          name="subCategoryId"
          label="Sub Category"
        />
      </Grid>
      <Grid item xs={12}>
        <Input
          type="Select"
          options={areaOfInterest}
          onChange={handleChange}
          value={state.orgAreaOfInterest}
          name="orgAreaOfInterest"
          label="Area Of Interest"
          multiple={true}
        />
      </Grid>
      <Grid item xs={3}>
        <Input
          type="Text"
          onChange={handleChange}
          value={state.minAge}
          name="minAge"
          label="Min Age"
        />
      </Grid>
      <Grid item xs={3}>
        <Input
          type="Text"
          onChange={handleChange}
          value={state.maxAge}
          name="maxAge"
          label="Max Age"
        />
      </Grid>
      <Grid item xs={6}></Grid>
      <Grid item xs={6}>
        <Input
          type="text"
          onChange={handleChange}
          value={state.hasBranch}
          name="hasBranch"
          label="Branch"
        />
      </Grid>
      <Grid item xs={6}></Grid>
      <Grid item xs={6}>
        <Input
          type="Select"
          options={orgEntities}
          onChange={handleChange}
          value={state.entityId}
          name="entityId"
          label="Entity"
        />
      </Grid>
      <Grid item xs={6}>
        <Input
          type="text"
          onChange={handleChange}
          value={state.registrationNumber}
          name="registrationNumber"
          label="Registration Number"
        />
      </Grid>
    </>
  );
};

export default SpecialInterest;
