import { Grid } from "@material-ui/core";
import React, { useState, useEffect } from "react";
import { businessType, orgEntities } from "../../../Constants/Constants";
import Input from "../../Form Components/Input";

const orgSubCategories = [
  {
    id: "4",
    name: "Private Limited",
    categoryId: "3",
  },
];
const Company = (props) => {
  const [state, setState] = useState({
    subCategoryId: 4,
    establishmentYear: null,
    businessTypeId: 1,
    noOfEmployees: null,
    industryHandled: null,
    entityId: 1,
    registrationNumber: null,
    year1: null,
    turnover1: null,
    year2: null,
    turnover2: null,
    year3: null,
    turnover3: null,
  });
  useEffect(() => {
    if(props.onClick===true){
      
    props.onChange(state, "company");
    }
    else if (props.data != null) {
      setState({ ...props.data });
      props.data.avgTurnover.length > 0 &&
        setState((prevState) => ({
            ...prevState,
          year1: props.data.avgTurnover[0].year,
          turnover1: props.data.avgTurnover[0].turnover,
          year2: props.data.avgTurnover[1].year,
          turnover2: props.data.avgTurnover[1].turnover,
          year3: props.data.avgTurnover[2].year,
          turnover3: props.data.avgTurnover[2].turnover,
        }));
        
    }
  }, [props.onClick]);
  const handleChange = (value, name) => {
    setState((prevState) => ({ ...prevState, [name]: value }));
    
    props.onChange(state, "company");
  };

  return (
    <>
      <Grid item xs={12}>
        <Input
          type="Select"
          options={orgSubCategories}
          onChange={handleChange}
          value={state.subCategoryId ? state.subCategoryId.name : null}
          name="orgSubCategory"
          label="Sub Category"
        />
      </Grid>
      <Grid item xs={6}>
        <Input
          type="text"
          onChange={handleChange}
          value={state.establishmentYear}
          name="establishmentYear"
          label="Year Of Establishment"
        />
      </Grid>
      <Grid item xs={6}>
        <Input
          type="Select"
          options={businessType}
          onChange={handleChange}
          value={state.businessTypeId}
          name="businessTypeId"
          label="Buisness Type"
        />
      </Grid>
      <Grid item xs={3}>
        <Input
          type="text"
          onChange={handleChange}
          value={state.noOfEmployees}
          name="noOfEmployees"
          label="No of Employees"
        />
      </Grid>
      <Grid item xs={9}>
        <Input
          type="text"
          onChange={handleChange}
          value={state.industryHandled}
          name="industryHandled"
          label="Industry Handled"
        />
      </Grid>
      <Grid item xs={6}>
        <Input
          type="Select"
          options={orgEntities}
          onChange={handleChange}
          value={state.entityId}
          name="entityId"
          label="Entity"
        />
      </Grid>
      <Grid item xs={6}>
        <Input
          type="text"
          onChange={handleChange}
          value={state.registrationNumber}
          name="registrationNumber"
          label="Registration Number"
        />
      </Grid>
      <Grid item xs={6}>
        Average Turnover Last 3 years
      </Grid>
      <Grid item xs={6}></Grid>
      <Grid item xs={6}>
        <Input
          type="text"
          onChange={handleChange}
          value={state.year1}
          name="year1"
          label="Year"
        />
      </Grid>
      <Grid item xs={6}>
        <Input
          type="text"
          onChange={handleChange}
          value={state.turnover1}
          name="turnover1"
          label="Turnover"
        />
      </Grid>
      <Grid item xs={6}>
        <Input
          type="text"
          onChange={handleChange}
          value={state.year2}
          name="year2"
          label="Year"
        />
      </Grid>
      <Grid item xs={6}>
        <Input
          type="text"
          onChange={handleChange}
          value={state.turnover2}
          name="turnover2"
          label="Turnover"
        />
      </Grid>
      <Grid item xs={6}>
        <Input
          type="text"
          onChange={handleChange}
          value={state.year3}
          name="year3"
          label="Year"
        />
      </Grid>
      <Grid item xs={6}>
        <Input
          type="text"
          onChange={handleChange}
          value={state.turnover3}
          name="turnover3"
          label="Turnover"
        />
      </Grid>
    </>
  );
};
export default Company;