import React, { useEffect } from 'react';
import { fetchOrgSetups, searchOrgSetup } from '../../../Api/Api';
import TableComponent from '../../Common/TableComponent';
import { ORG_BASIC_SETUP_FORM } from '../../Routes/RoutePath';

const columns = [
  { id: 'name', label: 'Organisation Name', minWidth: 170 },
  { id: 'orgCategory',label: 'Category',minWidth: 170 },
  { id: 'orgSubCategory',label: 'Sub Category',minWidth: 170 },
  { id: 'orgEntity',label: 'Entity',minWidth: 170 },

];

export default function ListingScreen(props) {
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [rows,setRows] = React.useState([]);
  useEffect(() => {
      if(props.search!=null){
        searchOrgSetupApi();
      }else{
        fetchOrgSetupsApi();
}
  },[rowsPerPage,props]);
  const searchOrgSetupApi =()=>{

    searchOrgSetup(props.search).then(({data})=>{
      console.log(data);
      setRows(data.rows);
  })
  }
  const fetchOrgSetupsApi =()=>{

    fetchOrgSetups(rowsPerPage,1).then(({data})=>{
       setRows(data.rows);
    })
  }

  return (
     <TableComponent rows={rows} columns={columns} link={ORG_BASIC_SETUP_FORM} page={(e)=>setRowsPerPage(e)}/> 
  );
}
