import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Popover from "@material-ui/core/Popover";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import { NavLink } from "react-router-dom";

const useStyles = makeStyles((theme) => ({
  sideNav: {
    border: "1px solid #707070",
    borderRadius: 0,
    width:"200px",
    height:"45px",

  },
  button: {
    marginLeft: theme.spacing(1),
    marginBottom: theme.spacing(1),
    marginTop: theme.spacing(1),
    width: "90%",
    height: "42px",
  },
  active: {
    backgroundColor: "grey",
  },
  text: {
    fontSize: "14px",
    fontColor:theme.colors.tableHeaderText,
  },
}));
export default function OrgSideNav(props) {
  const style = useStyles();
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);
  const id = open ? "simple-popover" : undefined;

  return (
    <div>
      <Button className={style.button} variant="outlined" onClick={handleClick}>
        <Typography className={style.text}>Organisations</Typography>
      </Button>
      <Popover
        id={id}
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{
          vertical: "top",
          horizontal: "right",
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "left",
        }}
      >
        <Typography>
          {" "}
          <Button
            className={style.sideNav}
            component={NavLink}
            to="/orgBasicSetup"
            fullWidth
          >
            {" "}
            BASIC SETUP
          </Button>
        </Typography>
        <Typography>
          <Button
            className={style.sideNav}
            component={NavLink}
            to="/orgBranches"
            fullWidth
          >
            BRANCHES
          </Button>
        </Typography>
        <Typography>
          <Button
            className={style.sideNav}
            component={NavLink}
            to="/orgClients"
            fullWidth
          >
            CLIENTS
          </Button>
        </Typography>
        <Typography>
          <Button
            className={style.sideNav}
            component={NavLink}
            to="/orgTestimonials"
            fullWidth
          >
            TESTIMONIALS
          </Button>
        </Typography>
        <Typography>
          <Button
            className={style.sideNav}
            component={NavLink}
            to="/orgGallaries"
            fullWidth
          >
            GALLARIES
          </Button>
        </Typography>
      </Popover>
    </div>
  );
}
