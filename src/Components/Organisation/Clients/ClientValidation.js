
export const ClientValidation = (data) => {
    for(let key in data){
        const value=data[key];
        if(key=="organisationId" && value==""){
            return false;
        }
        else if(key=="organisationId" && Number.isInteger(value)==false){
            return false;
        }
        else if(key=="logoUrl" && value==""){
            return false;
        }
        else if(key=="logoUrl" && value.match(/\.(jpeg|jpg|gif|png)$/) == null){
                return false;
        }
        else if(key=="name" && value==""){
                return false;
        }
    }
    return true;
}