import { Button, Grid, makeStyles, Typography } from "@material-ui/core";
import React, {  useState } from "react";
import { NavLink } from "react-router-dom";
import Download from "../../Common/Download";
import Search from "../../Common/Search";
import Filter from "../../Common/Filter";
import ClientListing from "./ClientListing";
import { ORG_CLIENTS_FORM } from "../../Routes/RoutePath";
import ButtonTable from "../../Common/ButtonTable";
const useStyles = makeStyles((theme) => ({
  spacing: {
    margin: theme.spacing(1),
  },
  search: {
    margin: theme.spacing(1),
    height: "10",
  },
}));
const ClientListContainer = (props) => {
  const[state,setState] = useState({
    search: null
  });

 const searchHandler = (val) => {
    setState((prevState)=>({...prevState, search: val }));
  };
    const styles = useStyles();
    return (
      <>
        <Grid container className={styles.spacing}>
          <Grid item xs={12}>
        <ButtonTable heading="CLIENTS" formLink={ORG_CLIENTS_FORM} search={searchHandler}/>
        </Grid>
        <Grid item xs={12}>
          <ClientListing search={state.search} />
          </Grid>
        </Grid>
      </>
    );
  
}
export default ClientListContainer;
