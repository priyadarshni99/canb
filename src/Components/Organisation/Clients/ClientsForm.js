import { Grid, Modal, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import React, { Component, useEffect, useState } from "react";
import { connect } from "react-redux";
import { useHistory } from "react-router";
import {
  createOrgClients,
  fetchOrgClientsById,
  fetchOrgSetups,
  updateOrgClients,
} from "../../../Api/Api";
import * as actionTypes from "../../../store/actions";
import FormHeading from "../../Common/FormHeading";
import Loading from "../../Common/Loading";
import FormContainer from "../../Form Components/FormContainer/FormContainer";
//import ImageInput from '../../Form Components/ImageInput'
import Input from "../../Form Components/Input";
import { ORG_CLIENTS } from "../../Routes/RoutePath";
import {ClientValidation} from "./ClientValidation";
const useStyles = makeStyles((theme) => ({
  paper: {
    backgroundColor:"white",
    width: 350,
    height:30,
    marginLeft: 400,
    marginTop:150,
    border:'none'
  },
  text:{
    backgroundColor:"white",
    border:'none',
    height:50,
    padding:15
  }
}));
const ClientsForm = (props) => {
  const history = useHistory();
  const [state, setState] = useState({
    organisationId: "",
    logoUrl: "",
    name: ""
  });
  const [organisations, setOrganisations]=useState([]);
  useEffect(() => {
    getData();
    if(props.match.params.edit){
      getEditData();
    }
  },[]);
  const getData =  async() => {
      await fetchOrgSetups(80, 1).then(({ data }) =>
      setOrganisations(data?.rows)
    )
    
  }
  const getEditData = async() => {
    await fetchOrgClientsById(props.match.params.id).then(({data}) => {
       setState((prevState)=>({
        ...prevState,
        organisationId: data.organisationId,
        logoUrl: data.logoUrl,
        name: data.name,
       }))
      }
    );
    }
  const createOrgData = async (data) => {
      if(ClientValidation(data)){
      await createOrgClients(data)
      .then((res) => history.push(ORG_CLIENTS));
      }else{
         props.error("Please Enter the data in the right format");
      }
  }
  const updateOrgData = async (data) => {
    if(ClientValidation(data)){
    await updateOrgClients(data, props.match.params.id)
    .then((res) => history.push(ORG_CLIENTS));
    }else{
       props.error("Please Enter the data in the right format");
    }
}  
  const handleChange = (value, name) => {
    setState({ ...state, [name]: value });
  };
  const handleClick = () => {
    let data = {...state};
    if (props.match.params.edit) {
      updateOrgData(data);
    } else {
      createOrgData(data);
    }
  };
  const styles = useStyles();  const a=props.error!=undefined? <Typography>{props.error}</Typography>:null

  return (
    <>
      <FormHeading heading="NEW CLIENT" link={ORG_CLIENTS} />
      <Modal
        open={props.modal}
        onClose={()=>props.modalClose()}
        className={styles.paper}>
          <Typography className={styles.text}>{props.errorMessage}</Typography>
      </Modal>
      
      <FormContainer onClick={handleClick} cancel={ORG_CLIENTS}>
        <Grid item xs={12}>
          <Input
            type="Select"
            options={organisations}
            name="organisationId"
            value={state.organisationId}
            onChange={handleChange}
            label="Organistions"
          />
        </Grid>
        <Grid item xs={4}>
          <Input
            type="Text"
            name="logoUrl"
            value={state.logoUrl}
            onChange={handleChange}
            label="Client Logo"
          />
        </Grid>
        <Grid item xs={8}>
          <Input
            type="Text"
            name="name"
            value={state.name}
            onChange={handleChange}
            label="Client Name"
          />
        </Grid>
      </FormContainer>
      
    </>
  );

};

const mapStateToProps=state=>{
  return{
  loading:state.loading,
  errorMessage:state.error,
  modal:state.modal
  };
  };
  const mapDispatchToProps= dispatch =>{
    return{
      modalClose: () => dispatch({type: actionTypes.MODAL_CLOSE}),
      error: (message) => dispatch({type: actionTypes.ERROR,message:message})
    };
    };
export default connect(mapStateToProps,mapDispatchToProps)(ClientsForm);
