import React, { useEffect } from "react";
import {
  fetchOrgClients,
  searchOrgClients,
} from "../../../Api/Api";
import TableComponent from "../../Common/TableComponent";
import { ORG_CLIENTS_FORM } from "../../Routes/RoutePath";

const columns = [
  { id: "organisation", label: "Institute Name", minWidth: 170 },
  { id: "name", label: "Client Name", minWidth: 170 },
];

export default function ClientListing(props) {
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [rows, setRows] = React.useState([]);
 
  useEffect(() => {
    if (props.search != null) {
      searchOrgClientsApi(props.search);
    } else {
      getOrgClients();
    }
  },[rowsPerPage,props]);
  const searchOrgClientsApi= async(search)=>{
    await searchOrgClients(search).then(({data}) => {
     setRows(data.rows);
   });
 }
 const getOrgClients = async() =>{
   await fetchOrgClients(rowsPerPage, 0).then((res) => {
     setRows(res.data.rows);
   });
 }

  return (
   <TableComponent rows={rows} columns={columns} link={ORG_CLIENTS_FORM} page={(e)=>setRowsPerPage(e)}/>
  );
}
