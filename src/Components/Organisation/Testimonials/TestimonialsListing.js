import React, { useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TablePagination from "@material-ui/core/TablePagination";
import TableRow from "@material-ui/core/TableRow";
import { fetchOrgTestimonial, searchOrgTestimonial } from "../../../Api/Api";
import { Box, Typography, Grid } from "@material-ui/core";
import Ellipses from "../../Common/Ellipses";

const columns = [
  { id: "organisation", label: "Institute Name", minWidth: 170 },
  { id: "orgClient", label: "Client Name", minWidth: 170 },
  { id: "testimonial", label: "Testimonial", minWidth: 170 },
  { id: "givenBy", label: "Given By", minWidth: 170 },
];
const useStyles = makeStyles({
  root: {
    width: "100%",
    margin: "2%",
  },
  container: {
    maxHeight: 440,
  },
});

export default function TestimonialListing(props) {
  const classes = useStyles();
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [rows, setRows] = React.useState([]);
  useEffect(() => {
    if (props.search != null) {
      searchOrgTestimonial(props.search).then((res) => {
        setRows(res.data.rows);
      });
    } else {
      let n = page * rowsPerPage;
      fetchOrgTestimonial(rowsPerPage, n).then((res) => {
        setRows(res.data.rows);
      });
    }
  });
  const handleChangePage = (event, newPage) => {
    console.log(newPage);
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  return (
    <Paper className={classes.root}>
      <TableContainer className={classes.container}>
        <Table stickyHeader aria-label="sticky table">
          <TableHead>
            <TableRow>
              {columns.map((column) => (
                <TableCell
                  key={column.id}
                  align={column.align}
                  style={{ minWidth: column.minWidth }}
                >
                  {column.label}
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {rows
              .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              .map((row) => {
                return (
                  <TableRow hover role="checkbox" tabIndex={-1} key={row.code}>
                    {columns.map((column) => {
                      const value =
                        typeof row[column.id] == "object"
                          ? row[column.id].name
                          : row[column.id];

                      if (column.id == "organisation") {
                        return (
                          <TableCell
                            key={column.id}
                            align={column.align}
                            style={{ display: "flex", flexWrap: "wrap" }}
                          >
                            <Grid container>
                              <Grid item xs={8}>
                                <Typography>
                                  {column.format && typeof value === "number"
                                    ? column.format(value)
                                    : value}
                                </Typography>
                              </Grid>
                              <Grid item xs={4}>
                                <Ellipses
                                  id={row.id}
                                  link="orgTestimonialForm"
                                  style={{ flex: 1 }}
                                />
                              </Grid>
                            </Grid>
                          </TableCell>
                        );
                      } else {
                        return (
                          <TableCell key={column.id} align={column.align}>
                            <Typography>
                              {column.format && typeof value === "number"
                                ? column.format(value)
                                : value}
                            </Typography>
                          </TableCell>
                        );
                      }
                    })}
                  </TableRow>
                );
              })}
          </TableBody>
        </Table>
      </TableContainer>

      <TablePagination
        rowsPerPageOptions={[5, 10, 25, 50, 75, 100]}
        component="div"
        count={rows.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onChangePage={handleChangePage}
        onChangeRowsPerPage={handleChangeRowsPerPage}
      />
    </Paper>
  );
}
