import { Button, Fab, Grid, makeStyles, Typography } from '@material-ui/core'
import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'
import Download from '../../Common/Download'
import Search from '../../Common/Search'
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import Filter from '../../Common/Filter'
import { withStyles } from '@material-ui/styles'
import TestimonialsListing from './TestimonialsListing'
const useStyles = theme => ({
  spacing: {
     margin: theme.spacing(1),
     
   },
   search:{
    margin: theme.spacing(1),
     height:"10"
   }
});
 class TestimonialsListContainer extends Component {
    state={
        search:null
    }
    
    searchHandler=(val)=>{
      this.setState({search:val})
    }
  render() {
    const {classes} = this.props;
    return (
      <>
        <Grid container className={classes.spacing}>
            <Grid item xs={3} >
              <Grid conatiner>
                <Grid item xs={4}><Fab style={{height: 5, width: 30}}><ArrowBackIcon/></Fab></Grid>
                <Grid item xs={8}><Typography>ORGANISATION</Typography></Grid>
              </Grid>
              </Grid>
            <Grid item xs={3} className={classes.spacing}></Grid>
            <Grid item xs={2} className={classes.search}><Search search={this.searchHandler}/></Grid>
            <Grid item xs={1} className={classes.spacing}><Filter/></Grid>
            <Grid item xs={1} className={classes.spacing}><Button component={NavLink} to="/orgTestimonialForm" style={{backgroundColor:"#00c853",color:"white"}}>New</Button></Grid>
            <Grid item xs={1} className={classes.spacing}><Download/></Grid>
            <TestimonialsListing search={this.state.search}/>
        </Grid>
      </>
    )
  }
}
export default  withStyles(useStyles)(TestimonialsListContainer);