import { Grid } from '@material-ui/core';
import { withStyles } from '@material-ui/styles'
import React, { Component } from 'react'
import Input from '../../Form Components/Input'
import FormContainer from '../../Form Components/FormContainer/FormContainer';
import { fetchOrgSetups, fetchOrgClients, createOrgTestimonial, updateOrgTestimonial, fetchOrgTestimonialById} from '../../../Api/Api';
import { countries, orgCatetories } from '../../../Constants/Constants';

const useStyles=(themes)=>({
    
});
 class Testimonials extends Component {
    state={
        organisationId:null,
        clientId:1,
        testimonial:"",
        givenBy:"",
        designation:"",
        organisations:[],
        clients:[]
    }
    handleChange=(value,name)=>{
            this.setState({[name]:value});
        
    }
    componentDidMount(){
      let data=[];
        fetchOrgSetups(50,1).then((res)=>{
            data=res.data.rows.map((org)=>{
                return(org)})
                this.setState((prevState)=>({...prevState,organisations:data}));
          })
          if(this.props.match.params.edit){
          fetchOrgTestimonialById(this.props.match.params.id).then((res)=>{
            console.log(res.data);
            this.setState({organisationId:res.data.organisationId,
              clientId:res.data.clientId,
              testimonial:res.data.testimonial,
              givenBy:res.data.givenBy,
              designation:res.data.designation})
       })
      }
    }
    handleChange=(value,name)=>{
      if(name=="organisationId"){
        fetchOrgClients(50,1).then((res)=>{
          let data=res.data.rows.filter((data)=>data.organisationId==value);
          this.setState({clients:data});
        })
      }
      this.setState({[name]:value})
    }
    handleClick=()=>{
      let data={
        organisationId:this.state.organisationId,
        clientId:this.state.clientId,
        testimonial:this.state.testimonial,
        givenBy:this.state.givenBy,
        designation:this.state.designation
      }
      if(this.props.match.params.edit){
        console.log(data);
        updateOrgTestimonial(data,this.props.match.params.id).then((res)=>alert("works")).catch((err)=>console.log(err));
      }else{
        console.log(data);
        createOrgTestimonial(data).then((res)=>alert("works")).catch((err)=>console.log(err));
      }
    }

  render() {
    return (
      <>
        <FormContainer onClick={this.handleClick}>
        <Grid item xs={12} >
             <Input type="Select" options={this.state.organisations} name="organisationId" label="Organisation Name" value={this.state.organisationId} onChange={this.handleChange} />
            </Grid>
        <Grid item xs={6} >
             <Input type="Text" name="clientLogo" label="Client Logo" onChange={this.handleChange} />
            </Grid>
            <Grid item xs={6} >
            <Input type="Select" options={this.state.clients} name="clientId" label="Client Name"  value={this.state.clientId}  onChange={this.handleChange}/>
            </Grid> 
            <Grid item xs={12} >
            <Input type="Text" label="Testimonial"  name="testimonial" value={this.state.testimonial} onChange={this.handleChange}/>
            </Grid>
            <Grid item xs={6} >
            <Input type="Text" label="Given By" name="givenBy" value={this.state.givenBy} onChange={this.handleChange}/>
            </Grid>
            <Grid item xs={6} >
            <Input type="Text" label="Designation" name="designation" value={this.state.designation}  onChange={this.handleChange}/>
            </Grid>
        </FormContainer>
      </>
    )
  }
}
export default withStyles(useStyles)(Testimonials);