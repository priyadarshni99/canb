import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import { Box } from "@material-ui/core";
const useStyles = makeStyles((theme) => ({
  paper: {
    top: "0px",
    left: "0px",
    margin:"0",
    width:"100%",
    height: "80px",
    border: "1px solid #707070",
    borderRadius: "0px 0px 20px 20px",
    opacity: 1,
  },
  heading:{
    fontSize: "20px",
  },
  textBox:{
    marginLeft:"52px",
    marginTop:"13px",
    width:"136px",
    height:"54px",
  },
  text: {
    fontSize: "12px",
  },
}));

const Logo = () => {
  const style = useStyles();
  return (
    <Box className={style.paper} color="primary">
      <Box className={style.textBox}>
        <Typography className={style.heading}>CANB</Typography>
        <Typography className={style.text}>Connect and Network</Typography>
        <Typography className={style.text}>Vhi Admin</Typography>
      </Box>
    </Box>
  );
};
export default Logo;
