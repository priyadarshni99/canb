
import { Box, FormControl, FormControlLabel, Grid, InputLabel, MenuItem, Paper, Select,Switch, TextField } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import React, { Component } from "react";
import FormContainer from "../../Form Components/FormContainer/FormContainer";
import SwitchComponent from "../../Form Components/SwitchComponent";


const useStyles = (theme) => ({
     spacing:{
         paddingBottom:theme.spacing(4)
     }

});

class ChatParameter extends Component {

    state={
        OrganisationName:{
            options:["1","2","3"],
            value:"",
            error:false,
            errorText:""
        },
        EnableChat:{
            checked:false
        }
    }
    handleChange=(e)=>{
        const value = e.target.type === 'checkbox' ? e.target.checked : e.target.value;
        this.setState((prevState)=>({
             ...prevState,
             [e.target.name]:{
                 ...prevState[e.target.name],
                 value:value
             }
        }))
    }
  render() {
   
    const { classes } = this.props;
    return (
        <FormContainer>
        <Grid item  xs={12}>
        <FormControl  variant="outlined" className={classes.spacing} fullWidth>
        <InputLabel htmlFor="outlined-age-native-simple">Organisation Name</InputLabel>
        <Select value={this.state.OrganisationName.value}
         onChange={this.handleChange} 
         error={this.state.OrganisationName.error}
         helperText={this.state.OrganisationName.errorText}
         label="Organisation Name"
         name="OrganisationName" >
        
        {
            this.state.OrganisationName.options.map((OrgName)=>{
                return(<MenuItem value={OrgName}>{OrgName}</MenuItem>)
            })
        }
        </Select>
      </FormControl>
      </Grid>
      <SwitchComponent label="Enable Chat For This Organisation" onChange={this.handleChange} checked={this.state.EnableChat.checked} />
            </FormContainer>
    )
}
}

export default withStyles(useStyles)(ChatParameter);