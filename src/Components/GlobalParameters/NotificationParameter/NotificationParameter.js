
import { Box, FormControl, FormControlLabel, Grid, InputLabel, MenuItem, Paper, Select,Switch, TextField } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import React, { Component } from "react";
import FormContainer from "../../Form Components/FormContainer/FormContainer";


const useStyles = (theme) => ({
     spacing:{
         paddingBottom:theme.spacing(4)
     }

});

class NotificationParameter extends Component {

    state={
        NotificationTo:{
            options:["User","Team","Group"],
            value:"",
            error:false,
            errorText:""
        }
    }
    handleChange=(e)=>{
        this.setState((prevState)=>({
             ...prevState,
             [e.target.name]:{
                 ...prevState[e.target.name],
                 value:e.target.value
             }
        }))
    }
  render() {
   
    const { classes } = this.props;
    return (
        <FormContainer>
        <Grid item  xs={12}>
        <FormControl  variant="outlined" className={classes.spacing} fullWidth>
        <InputLabel htmlFor="outlined-age-native-simple">Notification To</InputLabel>
        <Select value={this.state.NotificationTo.value}
         onChange={this.handleChange} 
         error={this.state.NotificationTo.error}
         helperText={this.state.NotificationTo.errorText}
         label="Organisation Name"
         name="NotificationTo" >
        
        {
            this.state.NotificationTo.options.map((NotifyTo)=>{
                return(<MenuItem value={NotifyTo}>{NotifyTo}</MenuItem>)
            })
        }
        </Select>
      </FormControl>
      </Grid>
      </FormContainer>
    )
}
}

export default withStyles(useStyles)(NotificationParameter);