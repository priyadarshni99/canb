import { Grid } from '@material-ui/core';
import { withStyles } from '@material-ui/styles'
import React, { Component } from 'react'
import FormContainer from '../../Form Components/FormContainer/FormContainer'
import Input from '../../Form Components/Input'
import OrganisationType from "./OrganisationType";
class Setup extends Component {
    state={
        Setup:{
            countries:{type:"Select",options:["India","Japan","China","USA"], value:"",error:"",errorText:"",label:"Countries"},
            areaOfInterest:{type:"Select",options:["Music","Dance","Chess","Batminton"], value:"",error:"",errorText:"",label:"Areas Of Interest"},
            BuisnessType:{type:"Select",options:["Sole proprietorship","partnership","LLC"], value:"",error:"",errorText:"",label:"Buisness Type"},
            examBoard:{type:"Select",options:["State Board","CBSE","ICSE"], value:"",error:"",errorText:"",label:"Exam Board"},
            entityType:{type:"Select",options:["Sole proprietorship","partnership","LLC"], value:"",error:"",errorText:"",label:"Entity Type"},
            reasonForDisable:{type:"text", value:"",error:"",errorText:"",label:"Reason For Disable"},
            relationship:{type:"text", value:"",error:"",errorText:"",label:"Relationship"},
            languages:{type:"Select",options:["Tamil","English","Hindi"], value:"",error:"",errorText:"",label:"Languages"},
        }
       
    }

    handleChange=(e,name)=>{
        const updatedObj=this.state.Setup;
        updatedObj[name].value=e.target.value;
        updatedObj[name].error=(e.target.value=="")?true:false;
        updatedObj[name].errorText=(e.target.value=="")?"Enter the value":"";
        this.setState((prevState)=>({...prevState,...updatedObj}));
      
    }
  render() {
   //   const {classes}=this.props;
   const formElementsArray = [];
   for (let key in this.state.Setup) {
       console.log(key);
       formElementsArray.push({...this.state.Setup[key],key:key});
   }

    return (
      <>
        <FormContainer>
          <Grid item xs={12}>
          {
              formElementsArray.map((input)=>{
                console.log(input.key);
                 return(<Input type={input.type} name={input.key}
                    options={input.options} value={input.value} 
                    error={input.error} errorText={input.errorText} 
                    label={input.label} onChange={this.handleChange}/>
                 ) 
              })
          }
          </Grid>
          <OrganisationType/>
        </FormContainer>
      </>
    )
  }
}
export default Setup;