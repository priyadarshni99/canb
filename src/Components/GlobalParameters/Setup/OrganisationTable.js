import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

const useStyles = makeStyles({
  tableHead: {
    backgroundColor: 650,
  },
});

export default function OrganisationTable(props) {
  const classes = useStyles();
 const OrgData=[];
 for (let key in props.OrgData ) {
    console.log(key);
    OrgData.push({...props.OrgData[key],key:key});
}
  return (
    <TableContainer component={Paper}>
      <Table aria-label="simple table">
        <TableHead color="secondary">
          <TableRow>
            <TableCell align="center">Organisation Category</TableCell>
            <TableCell align="center">Sub Category</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {OrgData.map((row) => (
            <TableRow>
              <TableCell align="center">{row.OrganisationType}</TableCell>
              <TableCell align="center">{row.SubCategory}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}