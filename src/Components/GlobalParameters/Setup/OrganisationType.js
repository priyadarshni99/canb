import { Fab,Grid,TextField, FormControl,InputLabel,MenuItem, Select } from '@material-ui/core'
import React, { Component } from 'react'
import AddIcon from '@material-ui/icons/Add';
import { withStyles } from '@material-ui/styles';
import OrganisationTable from './OrganisationTable';
const useStyles = theme => ({
    spacing: {
       paddingRight: theme.spacing(1),
       paddingTop: theme.spacing(1),
       paddingBottom: theme.spacing(1),
     },
  });
class OrganisationType extends Component {
    state={
        OrganisationType:{
            value:"",
            options:["Academic","Extracurricular"],
            error:false,
            errorText:"",
        },
        SubCategory:{
            value:"",
            options:["Academic","Extracurricular"],
            error:false,
            errorText:"",
        },
        ClassesHandled:{
            value:"",
            error:false,
            errorText:"",
        },
        Organisation:[],
    }
  
     
   handleChange=(e,name)=>{
       const updatedObj=this.state;
       updatedObj[name].value=e.target.value;
       updatedObj[name].error=e.target.value==""?true:false;
       updatedObj[name].errorText=e.target.value==""?"Enter the value":"";
       console.log(updatedObj);
       this.setState({...updatedObj});
   }
   handleClick=()=>{

       const obj={OrganisationType:this.state.OrganisationType.value,SubCategory:this.state.SubCategory.value,ClassesHandled:this.state.ClassesHandled.value};
       const Organisation=[...this.state.Organisation];
       Organisation.push(obj);
       this.setState({Organisation:{...Organisation}});
       this.setState({OrganisationType:{
        value:"",
        options:["Formal","Informal"],
        error:false,
        errorText:"",
    },
    SubCategory:{
        value:"",
        options:["Formal","Informal"],
        error:false,
        errorText:"",
    },
    ClassesHandled:{
        value:"",
        error:false,
        errorText:"",
    }})
   }
  render() {
    const {classes}=this.props;
    let Organisation=[];
    for (let key in this.state.Organisation ) {
        console.log(key);
        Organisation.push({...this.state.Organisation[key],key:key});
    }
    let createUI=Organisation.map((org, i) => {
        return(
            <><Grid item xs={6} className={classes.spacing}>
              <TextField fullWidth  label="Organisation Type" variant="outlined" value={org.OrganisationType}  /></Grid>
              <Grid item xs={6} className={classes.spacing}><TextField fullWidth   label="Sub Category" variant="outlined" value={org.SubCategory}  /></Grid>
              <Grid item xs={12} className={classes.spacing}><TextField fullWidth  label="Classes Handled" variant="outlined" value={org.ClassesHandled}  /></Grid>
            </>  )     
    } )
    return (
      <>
        {createUI}
        <Grid item xs={6} className={classes.spacing}>
            
          <FormControl  variant="outlined"  fullWidth>
          <InputLabel htmlFor="outlined-age-native-simple">OrganisationType</InputLabel>
           <Select  value={this.state.OrganisationType.value}  label="Organisation Type" 
             variant="outlined"  onChange={(e)=>this.handleChange(e,"OrganisationType")}
             error={this.state.OrganisationType.error} 
             helperText={this.state.OrganisationType.errorText}
              fullWidth>
        
        {
            this.state.OrganisationType.options.map((opt)=>{
                return(<MenuItem value={opt}>{opt}</MenuItem>)
            })
        }
        </Select>
        </FormControl>
        </Grid>
        <Grid item xs={6} className={classes.spacing}>
        <FormControl  variant="outlined"  fullWidth>
          <InputLabel htmlFor="outlined-age-native-simple">Sub Category</InputLabel>
           <Select  value={this.state.SubCategory.value}  label="Organisation Type" 
             variant="outlined"  onChange={(e)=>this.handleChange(e,"SubCategory")}
             error={this.state.SubCategory.error} 
             helperText={this.state.SubCategory.errorText}
              fullWidth>
        
        {
            this.state.SubCategory.options.map((opt)=>{
                return(<MenuItem value={opt}>{opt}</MenuItem>)
            })
        }
        </Select>
        </FormControl>
        </Grid>
        
        <Grid item xs={9} className={classes.spacing}>
        <TextField   label="Classes Handled" variant="outlined" value={this.state.ClassesHandled.value}  onChange={(e)=>this.handleChange(e,"ClassesHandled")} error={this.state.ClassesHandled.error} helperText={this.state.ClassesHandled.errorText} fullWidth  />
        </Grid>
        <Grid item xs={3}>
        <Fab color="primary" aria-label="add" onClick={this.handleClick}>
        <AddIcon />
        </Fab>
        </Grid>
        
        {(this.state.Organisation.length!=0) && <OrganisationTable OrgData={this.state.Organisation}/>}
              </>
    )
  }
}
export default withStyles(useStyles)(OrganisationType); 