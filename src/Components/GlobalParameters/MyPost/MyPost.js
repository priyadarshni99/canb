import { Grid } from '@material-ui/core'
import React, { Component } from 'react'
import FormContainer from '../../Form Components/FormContainer/FormContainer'
import Attributes from './Attributes'

export default class MyPost extends Component {
    state={
        Organisation:{

        },
        Students:{

        }
    }
    handleChange=(e,state,type)=>{
     this.setState({[type]:{...state}});
     console.log(this.state);
    }
  render() {
    return (
      <>
        <FormContainer>
            <Attributes heading="Organisations" onChange={(e,state)=>this.handleChange(e,state,"Organisation")} />
            <Attributes heading="Students" onChange={(e,state)=>this.handleChange(e,state,"Students")}/>
        </FormContainer>
      </>
    )
  }
}
