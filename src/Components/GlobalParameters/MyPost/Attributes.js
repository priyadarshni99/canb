import { FormControl, Grid, InputLabel, MenuItem, Select, TextField } from '@material-ui/core'
import { withStyles } from '@material-ui/styles'
import React, { Component } from 'react'
import FormContainer from '../../Form Components/FormContainer/FormContainer'
import InputSize from '../../Form Components/InputSize'
import SwitchComponent from '../../Form Components/SwitchComponent'
const useStyles = theme => ({
    spacing: {
       marginTop: theme.spacing(2),
       marginBottom: theme.spacing(2),
     },
  });
class Attributes extends Component {
    state={
        photoUploadAllowed:{value:true},
        PhotoWidth:{value:"", error:false,errorText:"" },
        PhotoHeight:{ value:"",error:false,errorText:""},
        BannerWidth:{value:"",error:false,errorText:""},
        BannerHeight:{ value:"",error:false,errorText:"" },
        maxUnrelatedContent:{ value:"",error:"",errorText:""},
        contentTypes:{value:"",options:["Articles","Audio","Video","Blogs"],error:false,errorText:""},
        gifUploadAllowed:{value:true},
        videoUploadAllowed:{value:true},
        maxVideoSize:{ value:"",error:false,errorText:""},
        audioUploadAllowed:{value:true},
        maxAudioSize:{ value:"",error:false,errorText:""}
    }
    handleChange=(e,name)=>{
        const obj={...this.state};
        const updatedObj=obj[name];
        if(e.target.checked){
            updatedObj.value=e.target.checked;
        }else{

        updatedObj.value=e.target.value;
        updatedObj.error=(e.target.value=="")?true:false;
        updatedObj.errorText=(e.target.value=="")?"Enter the value":"";
        }
        this.setState({[name]:{...updatedObj}});
       
        this.props.onChange(e,this.state);
    }
  render() {
    const {classes}=this.props;
    return (
      <>
        
            <Grid item xs={6} className={classes.spacing} >{this.props.heading}</Grid><Grid item xs={6}></Grid>
            <SwitchComponent className={classes.spacing} label="Photo Upload Allowed" onChange={(e)=>this.handleChange(e,"photoUploadAllowed")} checked={this.state.photoUploadAllowed.value}/>
            <Grid item xs={6}>
            <InputSize className={classes.spacing}  label="Photo size limitation" onchange={this.handleChange} name="Photo"
                 valueW={this.state.PhotoWidth.value} valueH={this.state.PhotoHeight.value}
                 errorW={this.state.PhotoWidth.error} errorH={this.state.PhotoHeight.error}
                 errorTextW={this.state.PhotoWidth.errorText} errorTextH={this.state.PhotoHeight.errorText}
                   />
            </Grid>
            <Grid item xs={6}>
                <InputSize className={classes.spacing}  label="Banner size limitation" onchange={this.handleChange} name="Photo"
                 valueW={this.state.BannerWidth.value} valueH={this.state.BannerHeight.value}
                 errorW={this.state.BannerWidth.error} errorH={this.state.BannerHeight.error}
                 errorTextW={this.state.BannerWidth.errorText} errorTextH={this.state.BannerHeight.errorText}
                   />
            </Grid>
            <TextField className={classes.spacing} label="Max Unrelated Content" variant="outlined" value={this.state.maxUnrelatedContent.value}  onChange={(e)=>this.handleChange(e,"maxUnrelatedContent")} error={this.state.maxUnrelatedContent.error} helperText={this.state.maxUnrelatedContent.errorText}   />
            <Grid item xs={6}></Grid>
            <FormControl className={classes.spacing} variant="outlined" fullWidth>
            <InputLabel id="demo-simple-select-outlined-label">Photo Transition</InputLabel>
            <Select    value={this.state.contentTypes.value}  label="Content Type"
             variant="outlined"  onChange={(e)=>this.handleChange(e,"contentTypes")}
             error={this.state.contentTypes.error} 
             helperText={this.state.contentTypes.errorText}
              fullWidth>
        
        {
            this.state.contentTypes.options.map((opt)=>{
                return(<MenuItem value={opt}>{opt}</MenuItem>)
            })
        }
        </Select>
        </FormControl>
            <SwitchComponent className={classes.spacing} label="Gif Upload Allowed" onChange={(e)=>this.handleChange(e,"gifUploadAllowed")} checked={this.state.gifUploadAllowed.value}/>
            <SwitchComponent className={classes.spacing} label="Video Upload Allowed" onChange={(e)=>this.handleChange(e,"videoUploadAllowed")} checked={this.state.videoUploadAllowed.value}/>
            <TextField className={classes.spacing} label="Max Video Size" variant="outlined" value={this.state.maxVideoSize.value}  onChange={(e)=>this.handleChange(e,"maxVideoSize")} error={this.state.maxVideoSize.error} helperText={this.state.maxVideoSize.errorText}   />
            <Grid item xs={6}></Grid>
            <SwitchComponent className={classes.spacing} label="Audio Upload Allowed" onChange={(e)=>this.handleChange(e,"audioUploadAllowed")} checked={this.state.audioUploadAllowed.value}/>
            <TextField className={classes.spacing} label="Max Audio Size" variant="outlined" value={this.state.maxAudioSize.value}  onChange={(e)=>this.handleChange(e,"maxAudioSize")} error={this.state.maxAudioSize.error} helperText={this.state.maxAudioSize.errorText}   />
            <Grid item xs={6}></Grid>
      </>
    )
  }
}
export default withStyles(useStyles)(Attributes);