import { Divider, Grid,FormControl, InputLabel, MenuItem, Select, Typography } from '@material-ui/core';
import { withStyles } from '@material-ui/styles';
import React, { Component } from 'react'
import FormContainer from '../../Form Components/FormContainer/FormContainer'
import InputSize from '../../Form Components/InputSize'

const useStyles = theme => ({
    spacing: {
       paddingTop: theme.spacing(2),
       paddingBottom:theme.spacing(2)
     },
  });

class MyPage extends Component {
    state={
        InstitutePhotoWidth:{
                value:"",
                error:false,
                errorText:""
            },
        InstitutePhotoHeight:{
                value:"",
                error:false,
                errorText:""
            },
            InstituteBannerWidth:{
                value:"",
                error:false,
                errorText:""
            },
        InstituteBannerHeight:{
                value:"",
                error:false,
                errorText:""
            },
            StudentPhotoWidth:{
                value:"",
                error:false,
                errorText:""
            },
        StudentPhotoHeight:{
                value:"",
                error:false,
                errorText:""
            },
            StudentBannerWidth:{
                value:"",
                error:false,
                errorText:""
            },
        StudentBannerHeight:{
                value:"",
                error:false,
                errorText:""
            },
        photoTransition:{
            value:"",
            options:["Fade In/Out", "Dissolves", "Wipes", "Whip Pan", "Zoom"],
            error:false,
            errorText:""

        }
    }
    handleChange=(e,name)=>{
        const obj={...this.state};
        const updatedObj=obj[name];
        updatedObj.value=e.target.value;
        updatedObj.error=(e.target.value=="")?true:false;
        updatedObj.errorText=(e.target.value=="")?"Enter the value":"";
        this.setState({[name]:{...updatedObj}});
       
        }
  render() {
    const {classes}=this.props;
    return (
      <>
        <FormContainer>
        <Grid item xs={6}>
            <Typography variant="h6">Institute</Typography>
            </Grid>
            <Grid item xs={6}></Grid>
            <Grid item xs={6}>
                <InputSize  label="Photo size limitation" onchange={this.handleChange} name="InstitutePhoto"
                 valueW={this.state.InstitutePhotoWidth.value} valueH={this.state.InstitutePhotoHeight.value}
                 errorW={this.state.InstitutePhotoWidth.error} errorH={this.state.InstitutePhotoHeight.error}
                 errorTextW={this.state.InstitutePhotoWidth.errorText} errorTextH={this.state.InstitutePhotoHeight.errorText}
                   />
            </Grid>
            <Grid item xs={6}>
                <InputSize label="Banner size limitation" onchange={this.handleChange} name="InstituteBanner"
                 valueW={this.state.InstituteBannerWidth.value} valueH={this.state.InstituteBannerHeight.value}
                 errorW={this.state.InstituteBannerWidth.error} errorH={this.state.InstituteBannerHeight.error}
                 errorTextW={this.state.InstituteBannerWidth.errorText} errorTextH={this.state.InstituteBannerHeight.errorText}
                   />
            </Grid>
            <Grid item xs={6}>
            <Typography variant="h6">Student</Typography>
            </Grid>
            <Grid item xs={6}></Grid>
            <Grid item xs={6}>
                <InputSize label="Photo size limitation" onchange={this.handleChange} name="StudentPhoto"
                 valueW={this.state.StudentPhotoWidth.value} valueH={this.state.StudentPhotoHeight.value}
                 errorW={this.state.StudentPhotoWidth.error} errorH={this.state.StudentPhotoHeight.error}
                 errorTextW={this.state.StudentPhotoWidth.errorText} errorTextH={this.state.StudentPhotoHeight.errorText}
                   />
            </Grid>
            <Grid item xs={6}>
                <InputSize label="Banner size limitation" onchange={this.handleChange} name="StudentBanner"
                 valueW={this.state.StudentBannerWidth.value} valueH={this.state.StudentBannerHeight.value}
                 errorW={this.state.StudentBannerWidth.error} errorH={this.state.StudentBannerHeight.error}
                 errorTextW={this.state.StudentBannerWidth.errorText} errorTextH={this.state.StudentBannerHeight.errorText}
                   />
            </Grid>
            <Divider/>

            <Grid item xs={12} className={classes.spacing}>
            <FormControl variant="outlined" fullWidth>
            <InputLabel id="demo-simple-select-outlined-label">Photo Transition</InputLabel>
            <Select  value={this.state.photoTransition.value}
             onChange={(e)=>this.handleChange(e,"photoTransition")} variant="outlined"
             error={this.state.photoTransition.error}
             helperText={this.state.photoTransition.errorText}
             label="Photo Transitions"
             name="photoTransition" fullWidth>
        
        {
            this.state.photoTransition.options.map((transitions)=>{
                return(<MenuItem value={transitions}>{transitions}</MenuItem>)
            })
        }
        </Select>
        </FormControl>
        </Grid>   
        </FormContainer>
      </>
    )
  }
}

export default withStyles(useStyles)(MyPage);