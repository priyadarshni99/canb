import React from 'react';
import Grid from '@material-ui/core/Grid';

import { Route,Switch } from 'react-router';
import Setup from './Setup/Setup';
import MyPage from './MyPage/MyPage';
import MyPost from './MyPost/MyPost';
import ProductParameter from './ProductParameter/ProductParameter';
import ChatParameter from './ChatParameter/ChatParameter';
import NotificationParameter from './NotificationParameter/NotificationParameter';
import StepperMenu from '../Stepper/StepperMenu';

function GlobalParameters() {
  return (
   
      <>
      <Grid container>
     <Grid item xs={2}><StepperMenu/> </Grid>
     <Grid item xs={1}></Grid>
     <Grid item xs={8}>
       <Switch>
       <Route path="/globalParam/Setup" component={Setup}/>
       <Route path="/globalParam/MyPage" component={MyPage}/>
       <Route path="/globalParam/MyPost" component={MyPost}/>
       <Route path="/globalParam/ProductParameter" component={ProductParameter}/>
       <Route path="/globalParam/ChatParameter" component={ChatParameter}/>
       <Route path="/globalParam/NotificationParameter" component={NotificationParameter}/>
       <Route path="/" component={Setup}/>
       </Switch>
     </Grid>
     </Grid>
     </>

  );
}

export default GlobalParameters;
