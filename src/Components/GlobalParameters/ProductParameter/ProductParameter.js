import { Divider, Grid, TextField,Switch} from '@material-ui/core';
import React, { Component } from 'react';
import { withStyles } from '@material-ui/styles';
import Paper from '@material-ui/core/Paper';
import FormContainer from "../../Form Components/FormContainer/FormContainer";
const useStyles = (theme => ({
  spacing: {
     marginTop: theme.spacing(1),
     marginBottom:theme.spacing(1),
   },
}));

 class ProductParameter extends Component {
    state={

        params:{
        WebApplication:{
            read:true,
            write:true,
            name:"Web Application"
        },
        MobileApplication:{
          read:true,
          write:false,
          name:"Mobile Application"
        },
        Dashboard:{
            read:false,
            write:false,
            name:"Dashboard"
        },
        Guests:{
          read:false,
          write:false,
          name:"GUESTS"
        },
        InvitedGuests:{
            read:true,
            write:true,
            name:"  Invited Guests"
        },
        AdhocGuests:{
            read:true,
            write:true,
            name:"  Adhoc Guests"
        },


        PlannedSingleVisitors:{
            read:false,
            write:false,
            name:"  Planned Single Visitors"
          },
          AdhocSingleVisitors:{
              read:true,
              write:true,
              name:"  Adhoc Single Visitors"
          },
          AdhocGroupVisitors:{
            read:true,
            write:true,
            name:"  Adhoc Group Visitors"
          },
          EmployeePass:{
              read:false,
              write:false,
              name:"  Employee Pass"
          },
          Inlocation:{
            read:false,
            write:false,
            name:"IN LOCATION"
          },
          ExitLocation:{
              read:false,
              write:false,
              name:"EXIT LOCATION"
          },
          LiveTracking:{
            read:false,
            write:false,
            name:"LIVE TRACKING"
          },
          DELIVERY:{
            read:true,
            write:true,
            name:"DELIVERY"
        },
        ReceiverMaster:{
          read:false,
          wrie:false,
          name:"  Receiver Master"
        },
        ItemMaster:{
            read:false,
            write:false,
            name:"  Item Master"
        },
        DeliveryChallan:{
          read:false,
          write:false,
          name:"  Delivery Challan"
        },
        DeliveryTracking:{
            read:true,
            write:true,
            name:"  Delivery Tracking"
        },
        IncidentReporting:{
          read:true,
          write:true,
          name:"  Incident Reporting"
        },
        Assignments:{
            read:false,
            write:false,
            name:"ASSIGNMENTS"
        },
        UserProfile:{
          read:false,
          write:false,
          name:"USER PROFILE"
        },
        Setup:{
            read:false,
            write:false,
            name:"  Setup"
        },
        Holidays:{
          read:false,
          write:false,
          name:"  Holidays"
        },
        UserAccess:{
            read:true,
            write:true,
            name:"  User Access"
        },
        Guards:{
          read:false,
          wrie:false,
          name:"  Guards"
        },
        Parameters:{
            read:false,
            write:false,
            name:"  Parameters"
        }
        }
    }

    handleChange=(e,key,action)=>{
        if(action=="read"){
          this.setState(prevState => ({
            ...prevState,
            params:{
            ...prevState.params,
            [key]:{
              ...prevState.params[key],
              read:!prevState.params[key].read
            }
          }
        }))
        }
        if(action=="write"){
          this.setState(prevState => ({
            ...prevState,
            params:{
            ...prevState.params,
            [key]:{
              ...prevState.params[key],
              write:!prevState.params[key].write
            }
          }
        }))
    }
  }
  render() {

    const {styles}=this.props;
    console.log(styles);
    const formElementsArray = [];
    for (let key in this.state.params) {
        formElementsArray.push({...this.state.params[key],key:key});
    }
    return (
      <>
        <FormContainer>
                <Grid item xs={12}><TextField id="outlined-basic" label="Outlined" variant="outlined" />
      </Grid>
<Divider variant="middle"/>
<Grid item xs={12}></Grid>
<Grid item xs={5}>Access</Grid>
<Grid item xs={1}>Read</Grid>
<Grid item xs={1}>Write</Grid>
<Grid item xs={5}></Grid>
{    
    formElementsArray.map((param,key)=>{
      //console.log(this.state.params);

          return(
              <>
            <Grid item xs={5}>{param.name}</Grid>
            <Grid item xs={1}>
            <Switch
            size="small"
        checked={this.state.params[param.key].read}
        onChange={(e)=>this.handleChange(e,param.key,"read")}
        color="primary"
        name="checkedB"
        inputProps={{ 'aria-label': 'primary checkbox' }}
      />
            </Grid>
            <Grid item xs={1}>
            <Switch
            size="small"

        checked={this.state.params[param.key].write}
        onChange={(e)=>this.handleChange(e,param.key,"write")}
        color="primary"
        name="checkedB"
        inputProps={{ 'aria-label': 'primary checkbox' }}
      />
            </Grid>
            <Grid item xs={5}>
                
            </Grid>
            </>
          )
         
    })
} </FormContainer>
      </>
    )
  }
}

export default withStyles(useStyles)(ProductParameter);