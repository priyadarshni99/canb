import React from 'react';

import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { Box } from '@material-ui/core';
const useStyles = makeStyles((theme) => ({
	header:{
    margin:"0",
    width:"100%",
    height: "64px",
    border: "1px solid #707070",
	padding:"0",
	backgroundolor:"#ffffff",
	float:"left"
	},
	text:{
		marginRight:theme.spacing(3),
		marginBottom:theme.spacing(2),
		fontSize:"14px",
		marginTop:"23px",
        fontStyle:theme.typography.headerFontFamily
	},
  }));
function Header(){
	
	
		const style=useStyles();
		const headerList=["OUR BILLINGS","AD CAMPAIGN","CONNECTION","ORG.CONTROL AREA"];
		return(

            <Box className={style.header}  position="static" color="default">
			<Toolbar>       
            {
            	headerList.map((item)=>{
                    return(<Typography className={style.text} variant="h6" >{item}</Typography>);
            	})
            }
			 </Toolbar>
            </Box>
			);
	
}
export default Header;
