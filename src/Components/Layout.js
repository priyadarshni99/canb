import { Grid, ThemeProvider } from "@material-ui/core";
import React, { useEffect } from "react";
import theme from "../Theme/Theme";
import Header from "./Header/Header";
import Logo from "./Logo/Logo";
import { routes } from "../Routes";
import SideNav from "./SideNav/SideNav";
import { Route, Switch } from "react-router";
import { connect } from "react-redux";
import Loading from "./Common/Loading";
function Layout(props) {
  console.log("I am layout");
  console.log("props ",props);
  return (
    <>
      <ThemeProvider theme={theme}>
       
      
       <Grid container>
          <Grid xs={2}>
            <Logo />
          </Grid>
          <Grid xs={10}>
            <Header />
          </Grid>
          <Grid xs={2}>
            <SideNav />
          </Grid>
          <Grid xs={10}>
          
            <Switch>
              
               { routes.map((rp, index) => {
                  return (
                    <Route
                      exact
                      path={rp.path}
                      component={rp.component}
                      key={index}
                    />
                  );
                })}
            </Switch>
           {props.loading && <Loading/>}
          </Grid>
        </Grid>
      </ThemeProvider>
    </>
  );
}
const mapStateToProps = (state) => {
  console.log("State ",state);
  return {
    loading: state.loading,
  };
};

export default connect(mapStateToProps)(Layout);
