import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Popover from '@material-ui/core/Popover';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import { MoreHoriz } from '@material-ui/icons';
import { Fab } from '@material-ui/core';
import { NavLink } from 'react-router-dom';

const useStyles = makeStyles((theme) => ({
  typography: {
    padding: theme.spacing(2),
  },
}));

export default function Ellipses(props) {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);
  const id = open ? 'simple-popover' : undefined;

  return (
    <div>
      <Button  style={{height: "24px", width: "24px",borderRadius:"100%"}} color="default" aria-describedby={id} onClick={handleClick}>
        <MoreHoriz style={{margin:"0px",padding:"0px"}} />
      </Button>
      <Popover
        id={id}
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'center',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
      >
      <Typography> <Button component={NavLink} to={`${props.link}/true/${props.id}`} fullWidth> Edit</Button></Typography>
       <Typography><Button component={NavLink} to={`${props.link}/false/${props.id}`} fullWidth>View Details</Button></Typography>
        <Typography><Button component={NavLink} to={`${props.link}/false/${props.id}`} fullWidth>InActive</Button></Typography>
      </Popover>
    </div>
  );
}