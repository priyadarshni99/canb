import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';
import { Grid } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    '& > * + *': {
      marginLeft: theme.spacing(2),
    },
  },
}));

export default function Loading() {
  const classes = useStyles();

  return (
      <>
    <Grid container>
    <Grid item  xs={5}></Grid>
    <Grid item xs={2}><CircularProgress thickness={5.5}/></Grid>
    <Grid item xs={5}></Grid>
    </Grid>
   </>
  );
}