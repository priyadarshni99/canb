import { Grid, Typography } from '@material-ui/core';
import React from 'react';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import { makeStyles } from '@material-ui/styles';
const useStyle=makeStyles((theme)=>({
    container:{
        fontSize:"20px",
        fontColor:"#333333",
        marginLeft:"40px",
        marginTop:"16px",
        paddingLeft:"20px"
    },
heading:{
    fontSize:"20px",
    fontColor:"#333333",
    paddingLeft:"30px",
    margin:"0px"
},
}));
export default function FormHeading(props) {
    const style=useStyle();
  return (
    <>
        <Grid container>
            <Grid xs={4}>
                <Grid container>
                    <Grid xs={1}>
                    <Typography className={style.container}><ArrowBackIcon /></Typography>
                    </Grid>
                    <Grid xs={11}>
                    <Typography className={style.container}>{props.heading}</Typography>
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
    </>
  );
}
