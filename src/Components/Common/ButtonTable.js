import { Button, Grid, makeStyles } from "@material-ui/core";

import React, { useState } from "react";
import { NavLink } from "react-router-dom";
import Download from "./Download";
import Filter from "./Filter";
import Heading from "./Heading";
import Search from "./Search";

const useStyles = makeStyles((theme) => ({
  spacing: {
    margin: theme.spacing(1),
  },
  search: {
    margin: theme.spacing(1),
    height: "10",
  },
  new: {
    backgroundColor: theme.colors.newButton,
    color: "white",
  },
}));
const ButtonTable = (props) => {
  const [state, setState] = useState({
    search: null,
  });

  const searchHandler = (val) => {
    setState({ search: val });
    props.search(state.search);
  };
  const styles = useStyles();
  return (
    <>
    <Grid container direction="row" alignContent="space-between">
      <Grid>
        <Heading>{props.heading}</Heading>
      </Grid>

      <Grid>
        <Search search={searchHandler} />
      </Grid>

      <Grid>
        <Filter />
      </Grid>

      <Grid>
        <Button
          style={{ height: "42px", width: "85px",marginLeft:"12px" }}
          component={NavLink}
          to={props.formLink}
          className={styles.new}
        >
          New
        </Button>
      </Grid>

      <Grid>
        <Download />
      </Grid>
      </Grid>
    </>
  );
};
export default ButtonTable;
