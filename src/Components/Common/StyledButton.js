import { styled } from "@material-ui/core/styles";
import { spacing } from "@material-ui/system";
import MuiButton from "@material-ui/core/Button";
import Button from "@material-ui/core/Button";

/**
 * Applies the spacing system to the material UI Button
 */
const StyledButton = styled(Button)(spacing);

export default StyledButton;