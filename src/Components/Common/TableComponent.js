import React, { useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TablePagination from "@material-ui/core/TablePagination";
import TableRow from "@material-ui/core/TableRow";
import { Typography, Grid } from "@material-ui/core";
import Ellipses from "./Ellipses";
const useStyles = makeStyles((theme) => ({
  root: {
    marginLeft: "40px",
    width: 900,
    margin: "2%",
  },
  container: {
    width: 900,
    maxHeight: 440,
  },
  tableHead: {
    padding: "0px",
    paddingLeft: "24px",
    height: "56px",
    backgroundColor: theme.colors.tableHeader_color,
    fontSize: "14px",
    fontStyle: theme.typography.fontFamily,
    color: theme.colors.tableHeaderText,
  },
  tableFooter: {
    width: 900,
    height: "52px",
    fontSize: "14px",
    fontStyle: theme.typography.fontFamily,
    backgroundColor: theme.colors.tableFooter,
  },
}));

const TableComponent = (props) => {
  const styles = useStyles();
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [rows, setRows] = React.useState([]);
  const [columns, setColumns] = React.useState([]);
  useEffect(() => {
    setRows(props.rows);
    setColumns(props.columns);
  }, [props]);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    props.page(event.target.value);
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  return (
    <Paper className={styles.root}>
      <TableContainer className={styles.container}>
        <Table stickyHeader>
          <TableHead>
            <TableRow>
              {columns.map((column) => (
                <TableCell
                  className={styles.tableHead}
                  key={column.id}
                  align={column.align}
                  style={{ minWidth: column.minWidth, fontSize: "14px" }}
                >
                  {column.label}
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {rows
              .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              .map((row) => {
                return (
                  <TableRow hover role="checkbox" tabIndex={-1} key={row.code}>
                    {columns.map((column) => {
                      const value =
                        typeof row[column.id] == "object"
                          ? row[column.id].name
                          : row[column.id];

                      if (column.id == columns[0].id) {
                        return (
                          <TableCell
                            key={column.id}
                            align={column.align}
                            style={{
                              display: "flex",
                              flexWrap: "wrap",
                              fontSize: "14px",
                              padding: "0px",
                              height: "56px",
                              paddingLeft: "24px",
                              paddingTop: "20px",
                              fontWeight: 900,
                            }}
                          >
                            <Grid container>
                              <Grid item xs={8}>
                                <Typography style={{ fontSize: "14px" }}>
                                  {column.format && typeof value === "number"
                                    ? column.format(value)
                                    : value}
                                </Typography>
                              </Grid>
                              <Grid item xs={4}>
                                <Ellipses
                                  id={row.id}
                                  link={props.link}
                                  style={{ flex: 1 }}
                                />
                              </Grid>
                            </Grid>
                          </TableCell>
                        );
                      } else {
                        return (
                          <TableCell
                            key={column.id}
                            align={column.align}
                            style={{
                              fontSize: "14px",
                              height: "56px",
                              fontWeight: 900,
                              padding: "0px",
                              paddingTop: "20px",
                              paddingLeft: "24px",
                            }}
                          >
                            <Typography style={{ fontSize: "14px" }}>
                              {column.format && typeof value === "number"
                                ? column.format(value)
                                : value}
                            </Typography>
                          </TableCell>
                        );
                      }
                    })}
                  </TableRow>
                );
              })}
          </TableBody>
        </Table>
      </TableContainer>

      <TablePagination
        className={styles.tableFooter}
        rowsPerPageOptions={[5, 10, 25, 50, 75, 100]}
        component="div"
        count={rows.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onChangePage={handleChangePage}
        onChangeRowsPerPage={handleChangeRowsPerPage}
      />
    </Paper>
  );
};
export default TableComponent;
