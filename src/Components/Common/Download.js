import { Button } from "@material-ui/core";
import { GetApp } from "@material-ui/icons";
import { makeStyles } from "@material-ui/styles";
import React, { Component } from "react";
import { downloadOrgBasic } from "../../Api/Api";
const useStyles = makeStyles((theme) => ({
  button: {
    padding: "0px",
    width: "42px",
    backgroundColor: theme.colors.downloadButton,
    height: "42px",
    marginLeft: "12px",
    color: "white",
  },
}));
const Download = (props) => {
  const styles = useStyles();
  const onClick = () => {
    downloadOrgBasic();
  };

  return (
    <>
      <Button className={styles.button} onClick={onClick}>
        <GetApp />
      </Button>
    </>
  );
};

export default Download;
