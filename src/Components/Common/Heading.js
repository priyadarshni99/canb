import { Typography } from '@material-ui/core'

import { makeStyles } from '@material-ui/styles';
const useStyles = makeStyles((theme) => ({
  heading:{
    fontSize:"20px",
    margin:"0px",
    width:"180px",
    fontColor:theme.colors.heading,
    fontFamily:theme.typography.heading,
    fontWeight:theme.typography.fontWeight,
    paddingLeft:"40px",
    paddingRight:"0px"
  }
}));
 const Heading = (props) => {
   const styles=useStyles();
    return (
      <>
        <Typography className={styles.heading}>{props.children}</Typography>
      </>
    )
  
}

export default Heading;