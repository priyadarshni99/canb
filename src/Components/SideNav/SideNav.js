import { Box, Button, Grid, makeStyles, Paper, Typography } from '@material-ui/core';
import { StylesContext } from '@material-ui/styles';
import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import OrgSideNav from '../Organisation/OrgSideNav';

const useStyles = makeStyles((theme) => ({
	sideNav:{
        top: "0px",
    left: "0px",
    margin:"0",
    width:"100%",
    border: "1px solid #707070",
    opacity: 1,
    },
	button:{
		marginLeft:theme.spacing(1),
        marginBottom:theme.spacing(1),
        marginTop:theme.spacing(1),
        height:"42px",
        width:"90%",
	},
    active:{
        backgroundColor:"grey",
    },
    text:{
        fontSize:"13px",
    }
  }));
const SideNav =() =>{


const style=useStyles();
		const sideNavList=["DASHBOARD","ORGANISATIONS","ORGANISATIONS GROUPS","ORG EMPLOYEES",
		 "ORGANISATIONS PAGES","ORGANISATION POSTS","ORGANISATION MEMBERS","NOTIFICATIONS",
		  "ACADEMY PERFORMANCE","OPINION POLL","ATTENDANCE","FINANCIALS","MEMBER PAGES"];
		return(
            
            <Grid container >
            <Box className={style.sideNav}>
            
            {
            sideNavList.map((item)=>{
                if(item=="ORGANISATIONS"){
                    return <OrgSideNav/>
                }
                else{
                    return (<>
                
                    <Button component={NavLink} to="/Organisation" className={style.button} variant="outlined">
                        <Typography className={style.text}>{item}</Typography>
                    </Button>
                
                </>
                )
                    }
            })
            
        }
            </Box>
            </Grid>
			);
	}

export default SideNav;