import React from 'react';
import { NavLink } from 'react-router-dom';
import style from './SideNavItems.css';
const SideNavItems=(props)=>{
	return(
          <div className="item">
          <NavLink to={props.link}  >
          {props.item}
          </NavLink>
          </div>
		);
};
export default SideNavItems;