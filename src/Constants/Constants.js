import { fetchOrgCategories,
    fetchOrgSubCategories, fetchOrgBoards, fetchOrgClassHandlersBySubCatId, fetchOrgEntities,
    fetchCountries, fetchStatesByCountry,fetchBusinessType,fetchGender,fetchRoles,fetchStatus,fetchCountryId,
    fetchOrganisationSeed,fetchUserType,fetchAreaOfInterest } from "../Api/Api";

let orgCategories=null;
let orgSubCategories=null;
let orgBoards=null;
let orgClassHandlersBySubCatId=null;
let orgEntities=null;
let countries=null;
let states=null;
let businessType=null;
let gender=null;
let roles=null;
let status=null;
let userType=null;
let areaOfInterest=null;
let organisations=null;

fetchOrgCategories().then((res)=>{
    orgCategories=res.data;
  return res.data;
  })
fetchOrgSubCategories(1).then((res)=>{
  orgSubCategories=res.data;
  return res.data;
  
})
fetchOrgBoards().then((res)=>{
    orgBoards=res.data;
  return res.data;
  
})
fetchOrgClassHandlersBySubCatId(1).then((res)=>{
    orgClassHandlersBySubCatId=res.data;
  return res.data;
  
})
fetchOrgEntities().then((res)=>{
    orgEntities=res.data;
  return res.data;
  
})
fetchCountries().then((res)=>{
    countries=res.data;
  return res.data;
  
})
fetchStatesByCountry().then((res)=>{
    states=res.data;
  return res.data;
  
})
fetchBusinessType().then((res)=>{
    businessType=res.data;
  return res.data;
  
})
fetchGender().then((res)=>{
    gender=res.data;
  return res.data;
  
})
fetchRoles().then((res)=>{
    roles=res.data;
  return res.data;
  
})
fetchStatus().then((res)=>{
    status=res.data;
  return res.data;
  
})
fetchUserType().then((res)=>{
    userType=res.data;
  return res.data;
  
})
fetchAreaOfInterest().then((res)=>{
  areaOfInterest=res.data;
  return res.data;
  
})
fetchOrganisationSeed().then((res)=>{
  organisations=res.data;
  return res.data;
})

export {orgCategories,orgSubCategories, orgBoards, orgClassHandlersBySubCatId, orgEntities,
    countries, states,businessType,gender,status,roles,userType,areaOfInterest,organisations};