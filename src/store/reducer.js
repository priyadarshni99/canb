import * as actionTypes from "./actions";

const initialState = {
  modal: false,
  loading: false,
  error: "",
};
let app={};
const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.REQUEST:
       app={
        ...state,
        error:"",
        loading: true,
        modal:false};
      return {
        ...app
      };
    case actionTypes.RESPONSE:
       app={...state,
        error:"",
        loading: false,
        modal:false};
      return {
        ...app
      };
      case actionTypes.ERROR:
         app={
          ...state,
          loading:false,
          error:action.message,
          modal: true};
      return {
        ...app
      };
    case actionTypes.MODAL_CLOSE:
       app={
        ...state,
        modal: false,
        error:"",
        loading:false};
      return {
        ...app
      };
  }
  return state;
};

export default reducer;
