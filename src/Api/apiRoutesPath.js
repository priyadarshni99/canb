const CREATE_WEB_THEME_PATH = "/createDetails/webThemeDetails";
const UPDATE_WEB_THEME_PATH = "/updateDetails/webThemeDetails";
const FETCH_WEB_THEMES_PATH = "/getAllDetailsWithCount/webThemeDetails";
const FETCH_WEB_THEME = "/getDetailById/webThemeDetails";
const DELETE_WEB_THEME = "/updateDetails/webThemeDetails";
const CREATE_MOBILE_THEME_PATH = "/createDetails/mobileThemeDetails";
const UPDATE_MOBILE_THEME_PATH = "/updateDetails/mobileThemeDetails";
const FETCH_MOBILE_THEMES_PATH = "/getAllDetailsWithCount/mobileThemeDetails";
const FETCH_MOBILE_THEME = "/getDetailById/mobileThemeDetails";
const DELETE_MOBILE_THEME = "/updateDetails/webThemeDetails";

// organisation
const CREATE_ORG_SETUP_PATH = "/createDetails/orgDetails";
const FETCH_ORG_SETUP_LIST_PATH = "/getAllDetailsWithCount/orgDetails";
const FETCH_ORG_SETUP_LIST_PATH_BY_ID = "/getDetailById/orgDetails";
const UPDATE_ORG_SETUP_PATH = "/updateDetails/orgDetails";
const ORG_BASIC_DOWNLOAD = "/downloadDetails/orgDetails";
// branches
const FETCH_ORG_BRANCHES_PATH = "getAllDetailsWithCount/branchDetails";
const FETCH_ORG_BRANCHES_BY_ID_PATH = "getDetailById/branchDetails";
const CREATE_ORG_BRANCHES_PATH = "createDetails/branchDetails";
const UPDATE_ORG_BRANCHES_PATH = "updateDetails/branchDetails";
const DOWNLOAD_ORG_BRANCHES_PATH = "downloadDetails/branchDetails";
//organisation employee
const CREATE_ORG_EMP_PATH = "/createDetails/userDetails";
const GET_ALL_ORG_EMP = "/getAllDetailsWithCount/userDetails";
const GET_EMP_BY_ID = "/getDetailById/userDetails";
const UPDATE_EMP_BY_ID = "/updateDetails/userDetails";
const DOWNLOAD_EMP_BY_TYPEID = "/downloadDetails/userDetails/2";

//branch
const GET_ALL_BRANCH = "/getAllDetailsWithCount/branchDetails";
//Clients
const FETCH_ORG_CLIENTS_PATH="getAllDetailsWithCount/clientDetails";
const CREATE_ORG_CLIENTS_PATH="createDetails/clientDetails";
const UPDATE_ORG_CLIENTS_PATH="updateDetails/clientDetails";
const FETCH_ORG_CLIENTS_BY_ID_PATH="/getDetailById/clientDetails";

//Testimonial
const FETCH_ORG_TESTIMONIAL_PATH="getAllDetailsWithCount/testimonialDetails";
const CREATE_ORG_TESTIMONIAL_PATH="createDetails/testimonialDetails";
const UPDATE_ORG_TESTIMONIAL_PATH="updateDetails/testimonialDetails";
const FETCH_ORG_TESTIMONIAL_BY_ID_PATH="/getDetailById/testimonialDetails";


// seed
const FETCH_CATEGORIES_PATH = `/getAllDetails/orgCatDetails`;
const FETCH_SUB_CATEGORIES_PATH = `/getChildren/orgSCatDetails/categoryId`;
const FETCH_BOARDS_PATH = `/getAllDetails/boardDetails`;
const FETCH_CLASS_HANDLER_BY_SUBCAT_ID_PATH = `/getChildren/classesDetails/subCategoryId`;
const FETCH_ORG_ENTITIES_PATH = `/getAllDetails/orgEntDetails`;
const FETCH_COUNRIES_PATH = `/getAllDetails/countryList`;
const FETCH_STATE_BY_COUN_PATH = `/getChildren/stateList/countryId/1`;
const FETCH_GENDER = "/getAllDetails/genderDetails";
const FETCH_ROLES = "/getAllDetails/roleDetails";
const FETCH_STATUS = "/getAllDetails/statusDetails";
const FETCH_BUSINESS_TYPE = `getAllDetails/orgBizDetails`;
const FETCH_USERTYPE='/getAllDetails/userTypeDetails';
const FETCH_AREA_OF_INTEREST='/getAllDetails/aoiDetails';
export {
  CREATE_WEB_THEME_PATH,
  FETCH_WEB_THEMES_PATH,
  FETCH_WEB_THEME,
  UPDATE_WEB_THEME_PATH,
  DELETE_WEB_THEME,
  CREATE_MOBILE_THEME_PATH,
  UPDATE_MOBILE_THEME_PATH,
  FETCH_MOBILE_THEMES_PATH,
  FETCH_MOBILE_THEME,
  DELETE_MOBILE_THEME,
  FETCH_CATEGORIES_PATH,
  FETCH_SUB_CATEGORIES_PATH,
  FETCH_BOARDS_PATH,
  FETCH_CLASS_HANDLER_BY_SUBCAT_ID_PATH,
  FETCH_ORG_ENTITIES_PATH,
  FETCH_STATE_BY_COUN_PATH,
  FETCH_COUNRIES_PATH,
  FETCH_ORG_SETUP_LIST_PATH,
  CREATE_ORG_SETUP_PATH,
  FETCH_BUSINESS_TYPE,
  FETCH_ORG_SETUP_LIST_PATH_BY_ID,
  UPDATE_ORG_SETUP_PATH,
  FETCH_ORG_BRANCHES_PATH,
  CREATE_ORG_BRANCHES_PATH,
  UPDATE_ORG_BRANCHES_PATH,
  FETCH_ORG_BRANCHES_BY_ID_PATH,
  FETCH_GENDER,
  FETCH_ROLES,
  FETCH_STATUS,
  CREATE_ORG_EMP_PATH,
  GET_ALL_ORG_EMP,
  GET_EMP_BY_ID,
  UPDATE_EMP_BY_ID,
  GET_ALL_BRANCH,
  DOWNLOAD_EMP_BY_TYPEID,
  ORG_BASIC_DOWNLOAD,
  DOWNLOAD_ORG_BRANCHES_PATH,
  FETCH_USERTYPE,
  FETCH_AREA_OF_INTEREST,
FETCH_ORG_CLIENTS_PATH,
CREATE_ORG_CLIENTS_PATH,
UPDATE_ORG_CLIENTS_PATH,
FETCH_ORG_CLIENTS_BY_ID_PATH,
FETCH_ORG_TESTIMONIAL_PATH,
CREATE_ORG_TESTIMONIAL_PATH,
UPDATE_ORG_TESTIMONIAL_PATH,
FETCH_ORG_TESTIMONIAL_BY_ID_PATH
};