
import axios from "axios";
import configureStore from "../store";
import * as actionTypes from "../store/actions";
import * as apiRoutesPath from './apiRoutesPath';
const store = configureStore();
const { dispatch } = store;
export const appApi =  axios.create({
  baseURL: process.env.REACT_APP_API_URL,
  responseType: "json",
  headers: {
    Accept: "application/json",
    "content-type": "application/json",
  },
});

appApi.interceptors.request.use((request)=>{
  dispatch({type:actionTypes.REQUEST});
  return request;
}, (error) => {
  dispatch({type:actionTypes.ERROR,message:"Something went Wrong!"})
  return Promise.reject(error);
});
appApi.interceptors.response.use((response)=>{
  dispatch({type:actionTypes.RESPONSE});
  return response;
}, (error) => {
  dispatch({type:actionTypes.ERROR,message:"Something went Wrong!"})
  return Promise.reject(error);
});
const createOrgBasicSetUp = (data) => appApi.post(`${apiRoutesPath.CREATE_ORG_SETUP_PATH}`, data);
const fetchOrgSetups = (limit, offset) => appApi.post(`${apiRoutesPath.FETCH_ORG_SETUP_LIST_PATH}/false/${limit}/${offset}`);
const fetchOrgSetupById = (id) =>
  appApi.get(`${apiRoutesPath.FETCH_ORG_SETUP_LIST_PATH_BY_ID}/${id}`);
const updateOrgBasicSetUp = (data, id) =>
  appApi.put(`${apiRoutesPath.UPDATE_ORG_SETUP_PATH}/${id}`, data);
const downloadOrgBasic = () =>
  appApi.get(`${apiRoutesPath.ORG_BASIC_DOWNLOAD}`);
const searchOrgSetup = (string) =>
  appApi.post(
    `${apiRoutesPath.FETCH_ORG_SETUP_LIST_PATH}/false?search=${string}`
  );
const filterOrgSetup = (limit, offset, data) =>
  appApi.post(
    `${apiRoutesPath.FETCH_ORG_SETUP_LIST_PATH}/false/${limit}/${offset}`,
    data
  );
//
const fetchOrgBranches = (limit, offset) =>
  appApi.post(
    `${apiRoutesPath.FETCH_ORG_BRANCHES_PATH}/false/${limit}/${offset}`
  );
const fetchOrgBranchesById = (id) =>
  appApi.get(`${apiRoutesPath.FETCH_ORG_BRANCHES_BY_ID_PATH}/${id}`);
const createOrgBranches = (data) =>
  appApi.post(`${apiRoutesPath.CREATE_ORG_BRANCHES_PATH}`, data);
const updateOrgBranches = (data, id) =>
  appApi.put(`${apiRoutesPath.UPDATE_ORG_BRANCHES_PATH}/${id}`, data);
const downloadOrgBranch = () =>
  appApi.get(`${apiRoutesPath.DOWNLOAD_ORG_BRANCHES_PATH}`);
const filterBranches = (data) =>
  appApi.post(`${apiRoutesPath.FETCH_ORG_BRANCHES_PATH}/false`, data);
const searchBranches = (string) =>
  appApi.post(
    `${apiRoutesPath.FETCH_ORG_BRANCHES_PATH}/false?search=${string}`
  );
//Clients
const createOrgClients = (data) => appApi.post(`${apiRoutesPath.CREATE_ORG_CLIENTS_PATH}`, data);
const fetchOrgClients = (limit, offset) => appApi.post(`${apiRoutesPath.FETCH_ORG_CLIENTS_PATH}/false/${limit}/${offset}`);
const fetchOrgClientsById = (id) =>
  appApi.get(`${apiRoutesPath.FETCH_ORG_CLIENTS_BY_ID_PATH}/${id}`);
const updateOrgClients = (data, id) =>
  appApi.put(`${apiRoutesPath.UPDATE_ORG_CLIENTS_PATH}/${id}`, data);
  const searchOrgClients = (string) =>
  appApi.post(
    `${apiRoutesPath.FETCH_ORG_CLIENTS_PATH}/false?search=${string}`
  );
  //Testimonial
  const createOrgTestimonial = (data) => appApi.post(`${apiRoutesPath.CREATE_ORG_TESTIMONIAL_PATH}`, data);
  const fetchOrgTestimonial = (limit, offset) => appApi.post(`${apiRoutesPath.FETCH_ORG_TESTIMONIAL_PATH}/false/${limit}/${offset}`);
  const fetchOrgTestimonialById = (id) =>
    appApi.get(`${apiRoutesPath.FETCH_ORG_TESTIMONIAL_BY_ID_PATH}/${id}`);
  const updateOrgTestimonial = (data, id) =>
    appApi.put(`${apiRoutesPath.UPDATE_ORG_TESTIMONIAL_PATH}/${id}`, data);
    const searchOrgTestimonial = (string) =>
    appApi.post(
      `${apiRoutesPath.FETCH_ORG_TESTIMONIAL_PATH}/false?search=${string}`
    );

// seed
const fetchOrgCategories = () => appApi.get(`${apiRoutesPath.FETCH_CATEGORIES_PATH}`);
const fetchOrgSubCategories = (id) => appApi.get(`${apiRoutesPath.FETCH_SUB_CATEGORIES_PATH}/${id}`);
const fetchOrgBoards = () => appApi.get(`${apiRoutesPath.FETCH_BOARDS_PATH}`);
const fetchOrgClassHandlersBySubCatId = (id) => appApi.get(`${apiRoutesPath.FETCH_CLASS_HANDLER_BY_SUBCAT_ID_PATH}/${id}`);
const fetchOrgEntities = () => appApi.get(`${apiRoutesPath.FETCH_ORG_ENTITIES_PATH}`);
const fetchAreaOfInterest = () => appApi.get(`${apiRoutesPath.FETCH_AREA_OF_INTEREST}`);
const fetchCountries = () => appApi.get(`${apiRoutesPath.FETCH_COUNRIES_PATH}`);
const fetchStatesByCountry = () => appApi.get(`${apiRoutesPath.FETCH_STATE_BY_COUN_PATH}`);
const fetchBusinessType = () =>appApi.get(`${apiRoutesPath.FETCH_BUSINESS_TYPE}`);
const fetchGender = () => appApi.get(`${apiRoutesPath.FETCH_GENDER}`);
const fetchRoles = () => appApi.get(`${apiRoutesPath.FETCH_ROLES}`);
const fetchStatus = () => appApi.get(`${apiRoutesPath.FETCH_STATUS}`);
const fetchCountryId = () => appApi.get(`${apiRoutesPath.FETCH_COUNRIES_PATH}`);
const fetchStateByCountryId = (id) =>appApi.get(`${apiRoutesPath.FETCH_STATE_BY_COUN_PATH}/${id}`);
const fetchOrganisationSeed = () =>appApi.get(`${apiRoutesPath.FETCH_ORG_SETUP_LIST_PATH}/true`);
const fetchUserType = () => appApi.get(`${apiRoutesPath.FETCH_USERTYPE}`);


export default appApi;
export{fetchOrgCategories,
  fetchOrgSubCategories, fetchOrgBoards, fetchOrgClassHandlersBySubCatId, fetchOrgEntities,
  fetchCountries, fetchStatesByCountry, fetchBusinessType,fetchGender,fetchRoles,fetchStatus,fetchCountryId,
  fetchOrganisationSeed,fetchUserType,createOrgBasicSetUp, fetchOrgSetups,fetchOrgSetupById,updateOrgBasicSetUp,
  downloadOrgBasic,searchOrgSetup,fetchAreaOfInterest,
  filterOrgSetup,fetchOrgBranches,fetchOrgBranchesById,createOrgBranches,updateOrgBranches,downloadOrgBranch,
  filterBranches,searchBranches,fetchStateByCountryId,createOrgClients,fetchOrgClients,fetchOrgClientsById,updateOrgClients,searchOrgClients,
  createOrgTestimonial,fetchOrgTestimonial,fetchOrgTestimonialById,updateOrgTestimonial,searchOrgTestimonial
};
