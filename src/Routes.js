import BasicSetupForm from "./Components/Organisation/BasicSetup/BasicSetupForm";
import ListContainer from "./Components/Organisation/BasicSetup/ListContainer";
import BranchListContainer from "./Components/Organisation/Branches/BranchListContainer";
import BranchForm from "./Components/Organisation/Branches/Form/BranchForm";
import ClientListContainer from "./Components/Organisation/Clients/ClientListContainer";
import ClientsForm from "./Components/Organisation/Clients/ClientsForm";
import TestimonialListContainer from "./Components/Organisation/Testimonials/TestimonialListContainer";
import TestimonialsForm from "./Components/Organisation/Testimonials/TestimonialsForm";
import * as routePath from "./Components/Routes/RoutePath";

const routes=[
    {
        path:routePath.ORG_BASIC_SETUP,
        component:ListContainer
    },
    {
        path:routePath.ORG_BASIC_SETUP_FORM,
        component:BasicSetupForm
    },
    {
        path:routePath.ORG_BASIC_SETUP_EDIT,
        component:BasicSetupForm
    },
    {
        path:routePath.ORG_BRANCH,
        component:BranchListContainer
    },
    {
        path:routePath.ORG_BRANCH_FORM,
        component:BranchForm
    },
    {
        path:routePath.ORG_BRANCH_EDIT,
        component:BranchForm
    },{
        path:routePath.ORG_CLIENTS,
        component:ClientListContainer
    },
    {
        path:routePath.ORG_CLIENTS_FORM,
        component:ClientsForm
    },
    {
        path:routePath.ORG_CLIENTS_EDIT,
        component:ClientsForm
    },
    {
        path:routePath.ORG_TESTIMONIALS,
        component:TestimonialListContainer
    },
    {
        path:routePath.ORG_TESTIMONIALS_FORM,
        component:TestimonialsForm
    },
    {
        path:routePath.ORG_TESTIMONIALS_EDIT,
        component:TestimonialsForm
    }

]
export {routes};