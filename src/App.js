import React from 'react';
import { Router,Switch,Route } from 'react-router';
import configureStore from './store';
import { createBrowserHistory } from "history";
import { Provider } from 'react-redux';
import Layout from './Components/Layout';
import * as ROUTE_PATH from './Components/Routes/RoutePath';
const store = configureStore();
const history = createBrowserHistory();
function App() {
  return (
    <Provider store={store} >
       <Layout />
  </Provider>
  );
}

export default App;
