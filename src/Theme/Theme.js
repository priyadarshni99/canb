import { createMuiTheme, makeStyles, ThemeProvider } from '@material-ui/core/styles';

const Theme = createMuiTheme({
    palette:{
       background:{
         paper:"#fff",
       },
    },
    typography:{
      fontFamily: "'Roboto', sans-serif",
      heading: "'Lato', sans-serif",
      headerFontFamily:"'Montserrat', sans-serif",
      fontWeight:900,
    },
    colors:{
      tableHeader_color:'#D2E1FC',
      tableFooter:'#F8FCFF',
      tableHeaderText:'#0000008A',
      tableRowText:'#000000DE',
      newButton:'#59B961',
      downloadButton:'#264DB5',
      elipses:'#264DB5',
      filterButton:'#E2E2E2',
      heading:'#333333'
    },
    fontSize:{
      buttons:"18px",
      text:"16px",

    }
    
  });

  export default Theme;